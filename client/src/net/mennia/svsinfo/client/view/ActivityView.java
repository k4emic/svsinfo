package net.mennia.svsinfo.client.view;

import java.awt.GridBagConstraints;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.mennia.svsinfo.client.controller.ActivityController;
import net.mennia.svsinfo.client.controller.NewsController;
import net.mennia.svsinfo.client.table.model.ActivityTableModel;
import net.mennia.svsinfo.client.view.helper.InactivityListener;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.NewsItem;
import net.mennia.svsinfo.utils.Config;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityView extends javax.swing.JFrame {

    private Activity selectedActivity;
    private ActivityTableModel tblModelFri;
    private ActivityTableModel tblModelSat;
    private ActivityTableModel tblModelSun;
    private NewsController newsController;
    private ActivityController activityController;
    private final Object activityLockObject;
    private final Object newsLockObject;

    /**
     * Creates new form ActivityFram
     */
    public ActivityView() {
        activityLockObject = new Object();
        newsLockObject = new Object();

        setUndecorated(true);
        initComponents();
        //setExtendedState(MAXIMIZED_BOTH);

        initTables();
        initControllers();

        loadActivities();
        loadNews();

        startInactivityListener();

        // hide details panel
        pnlDetails.setVisible(false);

        if (Config.getInstance().getBool(Config.Key.PREVENTSHUTDOWN)) {

            this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

            // close the window when CTRL + F12 is pressed.
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {

                @Override
                public boolean dispatchKeyEvent(KeyEvent e) {

                    if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_F12) {
                        dispose();
                        System.exit(0);
                    }

                    return false;
                }
            });
        }

    }

    private void startInactivityListener() {

        Action refreshDataAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                loadActivities();
                loadNews();
                reselectSelectedActivity();
            }
        };

        InactivityListener listener = new InactivityListener(refreshDataAction, 0, InactivityListener.USER_EVENTS);
        listener.setRepeats(true);
        int timeoutSeconds = Config.getInstance().getInt(Config.Key.INACTIVITYTIMEOUT);
        listener.setIntervalInMillis(timeoutSeconds * 1000);
        listener.start();
    }

    private void initControllers() {
        newsController = new NewsController();
        activityController = new ActivityController();
    }

    /**
     * Loads activities from the database and populates the respective tables
     * with the new information
     */
    private void loadActivities() {
        synchronized (activityLockObject) {
            List<Activity> activities = activityController.getActivitiesForCurrentEvent();
            populateTables(activities);
        }
    }

    /**
     * Loads news from the database and populates the respective panels with the
     * new information
     */
    private void loadNews() {
        synchronized (newsLockObject) {
            List<NewsItem> news = newsController.getRecentNews();
            drawNewsBox(news);
        }
    }

    private void drawNewsBox(List<NewsItem> newsItems) {
        drawNewsBox(newsItems, pnlNews);

        // scroll to top of news list
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                scrlNews.getVerticalScrollBar().setValue(0);
            }
        });
    }

    private void drawNewsBox(List<NewsItem> newsItems, JPanel canvasPanel) {
        GridBagConstraints gbc = getNewsItemsConstraint();

        int i = 0;
        int max = newsItems.size();
        for (NewsItem item : newsItems) {
            JPanel panel = new NewsListItem(item);
            canvasPanel.add(panel, gbc);

            // paints a seperator between each entry
            if (i + 1 < max) {
                canvasPanel.add(new JSeparator(), gbc);
            }

            i++;
        }

        // paints a fake component that takes up the remaining space in the scrollpane
        GridBagConstraints glue = getNewsItemsVerticalGlue();
        canvasPanel.add(Box.createVerticalGlue(), glue);
    }

    private GridBagConstraints getNewsItemsConstraint() {
        GridBagConstraints constraint = new GridBagConstraints();
        constraint.weightx = 1;
        constraint.weighty = 0;
        constraint.fill = GridBagConstraints.HORIZONTAL;
        constraint.gridwidth = GridBagConstraints.REMAINDER;

        return constraint;
    }

    private GridBagConstraints getNewsItemsVerticalGlue() {
        GridBagConstraints glue = new GridBagConstraints();
        glue.weighty = 1;

        return glue;
    }

    /**
     * Clears selection from all tables except the selected table
     *
     * @param selectedTable
     */
    public void clearSelections(JTable selectedTable) {
        if (selectedTable != tblFri) {
            tblFri.clearSelection();
        }

        if (selectedTable != tblSat) {
            tblSat.clearSelection();
        }

        if (selectedTable != tblSun) {
            tblSun.clearSelection();
        }
    }

    /**
     * Initializes activity tables with proper table models and selection
     * listeners
     */
    private void initTables() {

        tblModelFri = new ActivityTableModel();
        tblFri.setModel(tblModelFri);

        tblModelSat = new ActivityTableModel();
        tblSat.setModel(tblModelSat);

        tblModelSun = new ActivityTableModel();
        tblSun.setModel(tblModelSun);

        JTable[] activityTables = {tblFri, tblSat, tblSun};

        for (JTable table : activityTables) {
            table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.getSelectionModel().addListSelectionListener(new ActivityListSelectionListener(table));
            table.getTableHeader().setReorderingAllowed(false);
            table.getTableHeader().setResizingAllowed(false);

            // limit width of from/to time columns
            table.getColumnModel().getColumn(0).setMaxWidth(50);
            table.getColumnModel().getColumn(1).setMaxWidth(50);
        }


        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        JPanel selectedPanel;

        switch (day) {
            case Calendar.FRIDAY:
                selectedPanel = pnlFri;
                break;
            case Calendar.SATURDAY:
                selectedPanel = pnlSat;
                break;
            case Calendar.SUNDAY:
                selectedPanel = pnlSun;
                break;
            default:
                selectedPanel = pnlFri;
                break;
        }

        tbWeekdays.setSelectedComponent(selectedPanel);

    }

    /**
     * Displays activity details in the frame
     *
     * @param activity
     */
    public void showActivity(Activity activity) {

        if (activity == null) {
            return;
        }

        selectedActivity = activity;

        lblActivityName.setText(activity.getName());
        txtActivityDescription.setText(activity.getDescription());

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE 'kl.' HH:mm");

        String time = sdf.format(activity.getTimeOfStart());
        String place = activity.getRoom().getName();

        String timePlace = time + " - " + place;

        lblActivityTimePlace.setText(timePlace);

        showActivitySignup(activity);
        showNewsForActivity(activity);
        pnlDetails.setVisible(true);

    }

    private void showActivitySignup(Activity activity) {

        pnlSignupControls.setVisible(false);
        if (activity.getRequiresSignup()) {

            pnlSignupControls.setVisible(true);
            pnlSignupsOpen.setVisible(false);
            pnlSignupsClosed.setVisible(false);

            switch (activity.getSignupState()) {
                case OPEN:
                    pnlSignupsOpen.setVisible(true);
                    break;
                case CLOSED:
                    pnlSignupsClosed.setVisible(true);
                    break;
            }
        }
    }

    private void showNewsForActivity(Activity activity) {
        List<NewsItem> news = newsController.getNewsForActivity(activity);
        pnlNewsAbout.removeAll();
        if (news.isEmpty()) {
            pnlActivityNewsWrapper.setVisible(false);
        } else {
            drawNewsBox(news, pnlNewsAbout);

            // scroll to top of news list
            javax.swing.SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    scrlActivityNews.getVerticalScrollBar().setValue(0);
                }
            });

            pnlActivityNewsWrapper.setVisible(true);
        }
    }

    /**
     * Populates the activity tables with the given activities
     *
     * @param activities
     */
    private void populateTables(List<Activity> activities) {
        Calendar cal = Calendar.getInstance();

        tblModelFri.clear();
        tblModelSat.clear();
        tblModelSun.clear();

        Activity oldActivitySelection = selectedActivity;
        selectedActivity = null;

        for (Activity activity : activities) {
            Date start = activity.getTimeOfStart();
            cal.setTime(start);
            int day = cal.get(Calendar.DAY_OF_WEEK);

            ActivityTableModel model;

            switch (day) {
                case Calendar.FRIDAY:
                    model = tblModelFri;
                    break;

                case Calendar.SATURDAY:
                    model = tblModelSat;
                    break;

                case Calendar.SUNDAY:
                    model = tblModelSun;
                    break;

                default:
                    System.out.println("No valid table for activity: " + activity.getName());
                    continue;
            }

            model.addItem(activity);

            if (activity.equals(oldActivitySelection)) {
                selectedActivity = activity;
            }
        }

        tblModelFri.fireTableDataChanged();
        tblModelSat.fireTableDataChanged();
        tblModelSun.fireTableDataChanged();
    }

    /**
     * Select the currently selected activity following an update of the loaded
     * activities
     */
    private void reselectSelectedActivity() {
        if (selectedActivity != null) {
            Activity activity = selectedActivity;
            JTable table = getTableForActivity(activity);
            ActivityTableModel model = getTableModel(table);
            int index = model.indexOf(activity);
            table.setRowSelectionInterval(index, index);
        } else {
            pnlDetails.setVisible(false);
        }
    }

    private ActivityTableModel getTableModel(JTable table) {
        ActivityTableModel model = null;

        if (table == tblFri) {
            model = tblModelFri;
        } else if (table == tblSat) {
            model = tblModelSat;
        } else if (table == tblSun) {
            model = tblModelSun;
        }

        return model;
    }

    /**
     * Returns reference to the table this activity should reside in.
     *
     * @param activity
     * @return
     */
    private JTable getTableForActivity(Activity activity) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(activity.getTimeOfStart());

        JTable table = null;

        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.FRIDAY:
                table = tblFri;
                break;
            case Calendar.SATURDAY:
                table = tblSat;
                break;
            case Calendar.SUNDAY:
                table = tblSun;
                break;
        }

        return table;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        pnlActivitySchedule = new javax.swing.JPanel();
        tbWeekdays = new javax.swing.JTabbedPane();
        pnlFri = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblFri = new javax.swing.JTable();
        pnlSat = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblSat = new javax.swing.JTable();
        pnlSun = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblSun = new javax.swing.JTable();
        pnlDetails = new javax.swing.JPanel();
        lblActivityName = new javax.swing.JLabel();
        lblActivityTimePlace = new javax.swing.JLabel();
        txtActivityDescription = new javax.swing.JTextArea();
        pnlActivityNewsWrapper = new javax.swing.JPanel();
        lblNewsAbout = new javax.swing.JLabel();
        scrlActivityNews = new javax.swing.JScrollPane();
        pnlNewsAbout = new javax.swing.JPanel();
        pnlSignupControls = new javax.swing.JPanel();
        pnlSignupsOpen = new javax.swing.JPanel();
        lblActivityRequiresSignup = new javax.swing.JLabel();
        btnStartSignup = new javax.swing.JButton();
        pnlSignupsClosed = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        scrlNews = new javax.swing.JScrollPane();
        pnlNews = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Programoversigt");

        tblFri.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblFri.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblFri.setShowVerticalLines(false);
        tblFri.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(tblFri);

        javax.swing.GroupLayout pnlFriLayout = new javax.swing.GroupLayout(pnlFri);
        pnlFri.setLayout(pnlFriLayout);
        pnlFriLayout.setHorizontalGroup(
            pnlFriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
        );
        pnlFriLayout.setVerticalGroup(
            pnlFriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );

        tbWeekdays.addTab("Fredag", pnlFri);

        tblSat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblSat.getTableHeader().setReorderingAllowed(false);
        jScrollPane7.setViewportView(tblSat);

        javax.swing.GroupLayout pnlSatLayout = new javax.swing.GroupLayout(pnlSat);
        pnlSat.setLayout(pnlSatLayout);
        pnlSatLayout.setHorizontalGroup(
            pnlSatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
        );
        pnlSatLayout.setVerticalGroup(
            pnlSatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );

        tbWeekdays.addTab("Lørdag", pnlSat);

        tblSun.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        tblSun.getTableHeader().setReorderingAllowed(false);
        jScrollPane8.setViewportView(tblSun);

        javax.swing.GroupLayout pnlSunLayout = new javax.swing.GroupLayout(pnlSun);
        pnlSun.setLayout(pnlSunLayout);
        pnlSunLayout.setHorizontalGroup(
            pnlSunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
        );
        pnlSunLayout.setVerticalGroup(
            pnlSunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
        );

        tbWeekdays.addTab("Søndag", pnlSun);

        javax.swing.GroupLayout pnlActivityScheduleLayout = new javax.swing.GroupLayout(pnlActivitySchedule);
        pnlActivitySchedule.setLayout(pnlActivityScheduleLayout);
        pnlActivityScheduleLayout.setHorizontalGroup(
            pnlActivityScheduleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tbWeekdays, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        pnlActivityScheduleLayout.setVerticalGroup(
            pnlActivityScheduleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlActivityScheduleLayout.createSequentialGroup()
                .addComponent(tbWeekdays, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        lblActivityName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblActivityName.setText("navn");

        lblActivityTimePlace.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblActivityTimePlace.setText("tid og sted");

        txtActivityDescription.setBackground(new java.awt.Color(255, 255, 255, 0));
        txtActivityDescription.setColumns(20);
        txtActivityDescription.setEditable(false);
        txtActivityDescription.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtActivityDescription.setLineWrap(true);
        txtActivityDescription.setRows(2);
        txtActivityDescription.setWrapStyleWord(true);
        txtActivityDescription.setBorder(null);

        lblNewsAbout.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblNewsAbout.setText("Nyheder om aktiviteten");

        scrlActivityNews.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        pnlNewsAbout.setLayout(new java.awt.GridBagLayout());
        scrlActivityNews.setViewportView(pnlNewsAbout);

        javax.swing.GroupLayout pnlActivityNewsWrapperLayout = new javax.swing.GroupLayout(pnlActivityNewsWrapper);
        pnlActivityNewsWrapper.setLayout(pnlActivityNewsWrapperLayout);
        pnlActivityNewsWrapperLayout.setHorizontalGroup(
            pnlActivityNewsWrapperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlActivityNewsWrapperLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlActivityNewsWrapperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlActivityNewsWrapperLayout.createSequentialGroup()
                        .addComponent(lblNewsAbout)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(scrlActivityNews))
                .addContainerGap())
        );
        pnlActivityNewsWrapperLayout.setVerticalGroup(
            pnlActivityNewsWrapperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlActivityNewsWrapperLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblNewsAbout)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrlActivityNews))
        );

        lblActivityRequiresSignup.setText("Du skal tilmelde dig for at kunne deltage i aktiviteten");

        btnStartSignup.setText("Tilmeld");
        btnStartSignup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartSignupActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSignupsOpenLayout = new javax.swing.GroupLayout(pnlSignupsOpen);
        pnlSignupsOpen.setLayout(pnlSignupsOpenLayout);
        pnlSignupsOpenLayout.setHorizontalGroup(
            pnlSignupsOpenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSignupsOpenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblActivityRequiresSignup)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnStartSignup)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlSignupsOpenLayout.setVerticalGroup(
            pnlSignupsOpenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSignupsOpenLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSignupsOpenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblActivityRequiresSignup)
                    .addComponent(btnStartSignup))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setText("Tilmelding til aktiviteten er lukket");

        javax.swing.GroupLayout pnlSignupsClosedLayout = new javax.swing.GroupLayout(pnlSignupsClosed);
        pnlSignupsClosed.setLayout(pnlSignupsClosedLayout);
        pnlSignupsClosedLayout.setHorizontalGroup(
            pnlSignupsClosedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSignupsClosedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlSignupsClosedLayout.setVerticalGroup(
            pnlSignupsClosedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSignupsClosedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlSignupControlsLayout = new javax.swing.GroupLayout(pnlSignupControls);
        pnlSignupControls.setLayout(pnlSignupControlsLayout);
        pnlSignupControlsLayout.setHorizontalGroup(
            pnlSignupControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlSignupsOpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlSignupControlsLayout.createSequentialGroup()
                .addComponent(pnlSignupsClosed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlSignupControlsLayout.setVerticalGroup(
            pnlSignupControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSignupControlsLayout.createSequentialGroup()
                .addComponent(pnlSignupsOpen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlSignupsClosed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlDetailsLayout = new javax.swing.GroupLayout(pnlDetails);
        pnlDetails.setLayout(pnlDetailsLayout);
        pnlDetailsLayout.setHorizontalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDetailsLayout.createSequentialGroup()
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblActivityName, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblActivityTimePlace, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtActivityDescription, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlSignupControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(pnlActivityNewsWrapper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlDetailsLayout.setVerticalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblActivityName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblActivityTimePlace)
                .addGap(18, 18, 18)
                .addComponent(txtActivityDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlSignupControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlActivityNewsWrapper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nyheder");

        scrlNews.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrlNews.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        pnlNews.setLayout(new java.awt.GridBagLayout());
        scrlNews.setViewportView(pnlNews);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(pnlActivitySchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(scrlNews, javax.swing.GroupLayout.PREFERRED_SIZE, 473, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlActivitySchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrlNews, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE))
                    .addComponent(pnlDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartSignupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartSignupActionPerformed
        SignupView frame = new SignupView();
        frame.startSignup(selectedActivity);
        frame.setVisible(true);
    }//GEN-LAST:event_btnStartSignupActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnStartSignup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JLabel lblActivityName;
    private javax.swing.JLabel lblActivityRequiresSignup;
    private javax.swing.JLabel lblActivityTimePlace;
    private javax.swing.JLabel lblNewsAbout;
    private javax.swing.JPanel pnlActivityNewsWrapper;
    private javax.swing.JPanel pnlActivitySchedule;
    private javax.swing.JPanel pnlDetails;
    private javax.swing.JPanel pnlFri;
    private javax.swing.JPanel pnlNews;
    private javax.swing.JPanel pnlNewsAbout;
    private javax.swing.JPanel pnlSat;
    private javax.swing.JPanel pnlSignupControls;
    private javax.swing.JPanel pnlSignupsClosed;
    private javax.swing.JPanel pnlSignupsOpen;
    private javax.swing.JPanel pnlSun;
    private javax.swing.JScrollPane scrlActivityNews;
    private javax.swing.JScrollPane scrlNews;
    private javax.swing.JTabbedPane tbWeekdays;
    private javax.swing.JTable tblFri;
    private javax.swing.JTable tblSat;
    private javax.swing.JTable tblSun;
    private javax.swing.JTextArea txtActivityDescription;
    // End of variables declaration//GEN-END:variables

    /**
     * Listener class for activity tables
     */
    class ActivityListSelectionListener implements ListSelectionListener {

        JTable table;

        public ActivityListSelectionListener(JTable table) {
            this.table = table;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            int index = table.getSelectedRow();

            if (index == -1) {
                return; // Not a valid selection
            }

            ActivityTableModel model = (ActivityTableModel) table.getModel();

            Activity activity = model.getItemAt(index);

            showActivity(activity);
            clearSelections(table);

        }
    }
}
