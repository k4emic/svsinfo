package net.mennia.svsinfo.client.table.model;

import java.text.SimpleDateFormat;
import java.util.Collection;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.view.tablemodel.AbstractActivityTableModel;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityTableModel extends AbstractActivityTableModel {
    
    public static final int COL_START = 0;
    public static final int COL_END = 1;
    public static final int COL_NAME = 2;
    public static final int COL_ROOM = 3;

    private String[] colNames = {"Start", "Slut", "Navn", "Sted"};
    private Class[] colTypes = {String.class, String.class, String.class, String.class};

    public ActivityTableModel() {
        super();
    }
    
    public ActivityTableModel(Collection<Activity> items) {
        super(items);
    }

    @Override
    public int getColumnCount() {
        return colNames.length;
    }

    @Override
    public String getColumnName(int index) {
        return colNames[index];
    }

    @Override
    public Class getColumnClass(int index) {
        return colTypes[index];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Activity activity = getItemAt(rowIndex);
        
        String timeFormat = "HH:mm";
        SimpleDateFormat sdf;

        switch (columnIndex) {
            case COL_START:
                sdf = new SimpleDateFormat(timeFormat);
                return sdf.format(activity.getTimeOfStart());
            case COL_END:
                sdf = new SimpleDateFormat(timeFormat);
                return sdf.format(activity.getTimeOfEnd());
            case COL_NAME:
                return activity.getName();
            case COL_ROOM:
                return activity.getRoom().getName();
            default:
                return null;
        }
    }
    
}
