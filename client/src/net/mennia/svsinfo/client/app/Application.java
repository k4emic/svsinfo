package net.mennia.svsinfo.client.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import net.mennia.svsinfo.client.view.ActivityView;
import net.mennia.svsinfo.dbconnection.DBConnection;
import net.mennia.svsinfo.utils.Config;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Application {

    /**
     * @param args the command line arguments
     *
     * The command line arguments must be in the format {key}={value} The
     * following keys are accepted:
     *
     * Database: db.host The host to use for the database connection db.schema
     * The database schema to use in the application
     *
     * Other: debug Enables various debugging features, such as SQL logging.
     *
     */
    public static void main(String args[]) throws FileNotFoundException, IOException {

        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Application.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Application.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Application.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Application.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        loadConfiguration();
        configureDbConnection();

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new ActivityView().setVisible(true);
            }
        });
    }

    private static void loadConfiguration() throws FileNotFoundException, IOException {
        Config conf = Config.getInstance();

        File confFile = new File("client_config.properties");
        if (confFile.exists()) {
            conf.loadPropertiesFile(confFile);
        } else {
            System.out.println("Could not find config file. Using internal configuration.");
            InputStream stream = Application.class.getResourceAsStream("client_config.properties");
            try {
                conf.loadPropertiesStream(stream);
            } catch (IOException e) {
                throw e;
            } finally {
                stream.close();
            }
        }
    }

    private static void configureDbConnection() {
        Config conf = Config.getInstance();
        DBConnection conn = DBConnection.getInstance();

        conn.setHostname(conf.getString(Config.Key.DBHOST));
        conn.setUsername(conf.getString(Config.Key.DBUSER));
        conn.setPassword(conf.getString(Config.Key.DBPASSWORD));
        conn.setSchema(conf.getString(Config.Key.DBSCHEMA));
        conn.setDebug(conf.getBool(Config.Key.DBDEBUG));

    }
}
