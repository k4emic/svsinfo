package net.mennia.svsinfo.client.controller;

import java.util.List;
import net.mennia.svsinfo.dataaccess.ActivityDAO;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Event;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityController {

    public List<Activity> getActivitiesForCurrentEvent() {
        Event currentEvent = EventController.getCurrentEvent();
        
        ActivityDAO activityDao = new ActivityDAO();
        return activityDao.getActivitiesForEvent(currentEvent);
    }
}
