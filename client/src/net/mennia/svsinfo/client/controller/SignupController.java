package net.mennia.svsinfo.client.controller;

import net.mennia.svsinfo.dataaccess.SignupDAO;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Guest;
import net.mennia.svsinfo.model.Signup;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class SignupController {
    
    private Guest currentGuest;
    private Activity currentActivity;
    private Signup pendingSignup;
    
    
    public int countSignups(Activity activity) {
        SignupDAO dao = new SignupDAO();
        return dao.countSignups(activity);
    }
    
    public void createSignup(Activity activity, String username) throws NoSuchGuestException {
        
        // find guest
        Guest guest = new GuestController().findGuest(username);
        
        if(guest == null) {
            throw new NoSuchGuestException();
        }
        
        pendingSignup = new Signup(activity, guest);
    }
    
    public void createSignup(Activity activity, Guest guest) {
        pendingSignup = new Signup(activity, guest);
    }
    
    public void confirmSignup() {
        
        SignupDAO dao = new SignupDAO();
        
        dao.create(pendingSignup);
        pendingSignup = null;
    }
    
    public boolean hasFreeSlots(Activity activity) {
        if (! activity.getRequiresSignup()) {
            return true;
        }
        int max = activity.getMaxParticipants();
        int count = countSignups(activity);
        
        return max > count;
    }

    public Signup getPendingSignup() {
        return pendingSignup;
    }
    
}