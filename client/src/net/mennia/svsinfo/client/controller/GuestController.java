package net.mennia.svsinfo.client.controller;

import java.util.Date;
import net.mennia.svsinfo.dataaccess.GuestDAO;
import net.mennia.svsinfo.model.Guest;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class GuestController {

    public Guest findGuest(String username) {
        GuestDAO dao = new GuestDAO();
        return dao.findByUsername(username);
    }

    public Guest registerGuest(String username, String name, String gender, Date dob) {
        
        Guest g = new Guest();
        g.setUsername(username);
        g.setName(name);
        g.setGender(gender);
        g.setBirthdate(dob);
        
        GuestDAO dao = new GuestDAO();
        dao.insert(g);
        
        return g;
        
    }
}
