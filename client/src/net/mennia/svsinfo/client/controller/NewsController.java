package net.mennia.svsinfo.client.controller;

import java.util.LinkedList;
import java.util.List;
import net.mennia.svsinfo.dataaccess.NewsDAO;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.NewsItem;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class NewsController {

    private NewsDAO dao;
    private List<NewsItem> recentNews;

    public NewsController() {
        dao = new NewsDAO();
    }

    public List<NewsItem> getRecentNews() {
        recentNews = dao.findAll(EventController.getCurrentEvent());
        return recentNews;
    }

    public List<NewsItem> getNewsForActivity(Activity activity) {
        assert activity != null;
        List<NewsItem> list = new LinkedList<NewsItem>();
        for(NewsItem item : recentNews) {
            if(activity.equals(item.getActivity())) {
                list.add(item);
            }
        }

        return list;
    }


}
