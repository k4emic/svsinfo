package net.mennia.svsinfo.client.controller;

import java.util.HashMap;
import java.util.Map;
import net.mennia.svsinfo.dbconnection.DBConnection;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ApplicationController {

    public void parseArguments(String[] args) {
        
        Map<String, String> config = new HashMap();
        
        for(String arg : args) {
            String[] parts = arg.split("=");
            config.put(parts[0], parts[1]);
        }
        
        if(config.containsKey("db.host")) {
            DBConnection.getInstance().setHostname(config.get("db.host"));
        }
        
        if(config.containsKey("db.schema")) {
            DBConnection.getInstance().setSchema(config.get("db.schema"));
        }
        
        if(config.containsKey("debug")) {
            if(config.get("debug").equals("true")) {
                DBConnection.getInstance().setDebug(true);
            }
            
        }
        
    }
    
}
