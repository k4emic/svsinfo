package net.mennia.svsinfo.client.controller;

import net.mennia.svsinfo.dataaccess.EventDAO;
import net.mennia.svsinfo.model.Event;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class EventController {

    private static Event currentEvent = null;
    
    public static Event getCurrentEvent() {

        if(currentEvent == null) {
            currentEvent = new EventDAO().getLatestEvent();
        }

        return currentEvent;
        
    }
}
