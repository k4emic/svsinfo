package controller;

import net.mennia.svsinfo.client.controller.SignupController;
import net.mennia.svsinfo.model.Activity;
import org.junit.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class SignupControllerTest {
    
    public SignupControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of hasFreeSlots method, of class SignupController.
     */
    @Test
    public void testHasFreeSlots() {
        System.out.println("hasFreeSlots");
        
        Activity activity = new Activity();
        activity.setId(6);
        activity.setRequiresSignup(true);
        activity.setMaxParticipants(20);
        
        SignupController signup = new SignupController();
        
        assertTrue(signup.hasFreeSlots(activity));
        
        activity.setId(42);
        activity.setRequiresSignup(true);
        activity.setMaxParticipants(0);
        
        assertFalse(signup.hasFreeSlots(activity));
    }
}
