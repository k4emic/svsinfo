package net.mennia.svsinfo.admin.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import net.mennia.svsinfo.admin.view.ActivityAdmin;
import net.mennia.svsinfo.dbconnection.DBConnection;
import net.mennia.svsinfo.utils.Config;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Application {

    /**
     * @param args the command line arguments
     *
     * The command line arguments must be in the format {key}={value} The
     * following keys are accepted:
     *
     * Database: db.host The host to use for the database connection db.schema
     * The database schema to use in the application
     *
     * Other: debug Enables various debugging features, such as SQL logging.
     *
     */
    public static void main(String args[]) throws FileNotFoundException, IOException {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }

        loadConfiguration();
        configureDbConnection();

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame frame = new ActivityAdmin();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    private static void loadConfiguration() throws FileNotFoundException, IOException {
        Config conf = Config.getInstance();

        String configFileName = "admin_config.properties";

        File confFile = new File(configFileName);
        if (confFile.exists()) {
            conf.loadPropertiesFile(confFile);
        } else {
            System.out.println("Could not find config file. Using internal configuration.");
            InputStream stream = Application.class.getResourceAsStream(configFileName);
            try {
                conf.loadPropertiesStream(stream);
            } catch (IOException e) {
                throw e;
            } finally {
                stream.close();
            }
        }
    }

    private static void configureDbConnection() {
        Config conf = Config.getInstance();
        DBConnection conn = DBConnection.getInstance();

        conn.setHostname(conf.getString(Config.Key.DBHOST));
        conn.setUsername(conf.getString(Config.Key.DBUSER));
        conn.setPassword(conf.getString(Config.Key.DBPASSWORD));
        conn.setSchema(conf.getString(Config.Key.DBSCHEMA));
        conn.setDebug(conf.getBool(Config.Key.DBDEBUG));

    }

}
