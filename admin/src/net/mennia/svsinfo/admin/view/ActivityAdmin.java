package net.mennia.svsinfo.admin.view;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.mennia.svsinfo.admin.controller.ActivityController;
import net.mennia.svsinfo.admin.controller.EventController;
import net.mennia.svsinfo.admin.controller.RoomController;
import net.mennia.svsinfo.admin.view.tablemodel.AdminActivityTableModel;
import net.mennia.svsinfo.helpers.date.DateHelper;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Event;
import net.mennia.svsinfo.model.Room;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityAdmin extends javax.swing.JFrame {

    private ActivityController controller;
    private AdminActivityTableModel tableModel;
    private Activity selectedActivity;
    private Event event;

    /**
     * Creates new form ActivityAdmin
     */
    public ActivityAdmin() {

        controller = new ActivityController();
        event = EventController.getCurrentEvent();

        initComponents();
        onRequiresSignupChange();

        SpinnerModel sm = new SpinnerNumberModel(1, 1, 2000, 1);
        spnMaxParticipants.setModel(sm);

        unselectActivity();

        initTable();
        attachTableListeners();

        loadRooms();
        beginLoadActivities();

        radioFriday.setSelected(true);
    }

    private void loadRooms() {

        RoomController roomContr = new RoomController();

        List<Room> rooms = roomContr.getRoomsForEvent(event);

        for (Room room : rooms) {
            cbRooms.addItem(room);
        }
    }

    private void beginLoadActivities() {

        Runnable run = new Runnable() {

            @Override
            public void run() {
                lblLoading.setVisible(true);
                loadActivities();
                lblLoading.setVisible(false);
            }
        };

        Thread t = new Thread(run);
        t.start();

    }

    private void loadActivities() {
        List<Activity> activities = controller.getActivitiesForEvent(event);
        tableModel.setItems(activities);
        tableModel.fireTableDataChanged();
    }

    private void initTable() {
        tableModel = new AdminActivityTableModel();
        tblActivities.setModel(tableModel);
        tblActivities.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblActivities.setAutoCreateRowSorter(true);
    }

    private void attachTableListeners() {

        // listen for activity selections and call the delegate method
        ListSelectionListener selectionListener = new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {

                int viewIndex = tblActivities.getSelectedRow();
                if (!e.getValueIsAdjusting() && viewIndex >= 0) {
                    int modelIndex = tblActivities.convertRowIndexToModel(viewIndex);

                    Activity act = tableModel.getItemAt(modelIndex);

                    // update table
                    onSelectActivity(act);
                }
            }
        };

        tblActivities.getSelectionModel().addListSelectionListener(selectionListener);

        // listen for changes to the checkbox
        chkRequiresSignup.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                onRequiresSignupChange();
            }
        });
    }

    /**
     * Populates the form with the given activity.
     *
     * @param activity
     */
    private void onSelectActivity(Activity activity) {

        if (!btnSave.isEnabled()) {
            btnSave.setEnabled(true);
        }

        if (!btnDelete.isEnabled()) {
            btnDelete.setEnabled(true);
        }

        if (activity.getRequiresSignup() == true) {
            btnOpenSignup.setEnabled(true);
        } else {
            btnOpenSignup.setEnabled(false);
        }

        selectedActivity = activity;

        if (activity != null) {

            txtName.setText(activity.getName());
            txtDescription.setText(activity.getDescription());
            txtDuration.setText(String.valueOf(activity.getDuration()));
            chkRequiresSignup.setSelected(activity.getRequiresSignup());
            spnMaxParticipants.setValue(activity.getMaxParticipants());

            Date startDate = activity.getTimeOfStart();

            DateFormat dateFormat = SimpleDateFormat.getDateInstance(DateFormat.SHORT);

            setDay(startDate);

            DateFormat timeFormat = SimpleDateFormat.getTimeInstance(DateFormat.SHORT);
            String timeString = timeFormat.format(startDate);

            txtTimeStart.setText(timeString);

            cbRooms.setSelectedItem(activity.getRoom());

        }

    }

    /**
     * Called whenever the checkbox for "Requires Signup" is toggled. Decides
     * whether or not the field for max participants should be enabled.
     */
    private void onRequiresSignupChange() {
        if (chkRequiresSignup.isSelected()) {
            spnMaxParticipants.setEnabled(true);
        } else {
            spnMaxParticipants.setEnabled(false);
        }
    }

    private Room getSelectedRoom() {
        return (Room) cbRooms.getSelectedItem();
    }

    private void unselectActivity() {
        selectedActivity = null;
        btnSave.setEnabled(false);
        btnDelete.setEnabled(false);
        btnOpenSignup.setEnabled(false);
    }

    private void setDay(Date day) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(day);

        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.FRIDAY:
                radioFriday.setSelected(true);
                break;
            case Calendar.SATURDAY:
                radioSaturday.setSelected(true);
                break;
            case Calendar.SUNDAY:
                radioSunday.setSelected(true);
                break;
            default:
                throw new IndexOutOfBoundsException("The specified day of week is not supported.");
        }
    }

    private int getSelectedDay() {

        int value = -1;

        if (radioFriday.isSelected()) {
            value = Calendar.FRIDAY;
        } else if (radioSaturday.isSelected()) {
            value = Calendar.SATURDAY;
        } else if (radioSunday.isSelected()) {
            value = Calendar.SUNDAY;
        }

        return value;
    }

    /**
     * Retrieves the date from the selected day in comparison to the start of
     * the event
     *
     * @return
     */
    private Date getSelectedDate() {

        Calendar cal = Calendar.getInstance();
        cal.setTime(event.getStartTime()); // set time to start on the day the convention starts.

        int selected = getSelectedDay();
        cal.set(Calendar.DAY_OF_WEEK, selected); // change the day of week, which might also change the date.

        return cal.getTime();
    }

    private int getDuration() throws NumberFormatException {
        int val = 0;

        try {
            val = Integer.parseInt(txtDuration.getText());
        } catch(NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Systemet kunne ikke tolke varigheden.", null, JOptionPane.ERROR_MESSAGE);
            throw e;
        }

        return val;
        
    }

    private int getMaxParticipants() {
        return (Integer) spnMaxParticipants.getValue();
    }

    private Date getStartTime() throws ParseException {
        String timeString = txtTimeStart.getText();

        DateFormat timeformat = DateFormat.getTimeInstance(DateFormat.SHORT);
        Date time = timeformat.parse(timeString);

        return time;
    }

    private Date getStartDateTime() throws ParseException {
        Date date = getSelectedDate();
        
        Date time = null;
        try {
            time = getStartTime();
        } catch(ParseException e) {
            JOptionPane.showMessageDialog(this, "Systemet kunne ikke tolke den indtastede tid.", null, JOptionPane.ERROR_MESSAGE);
            throw e;
        }

        return DateHelper.combineDateTime(date, time);
    }

    private void resetInput() {
        txtName.setText("");
        txtDescription.setText("");
        txtTimeStart.setText("");
        txtDuration.setText("");
        chkRequiresSignup.setSelected(false);
        onRequiresSignupChange();
        spnMaxParticipants.setValue(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrpDay = new javax.swing.ButtonGroup();
        pnlForm = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescription = new javax.swing.JTextArea();
        lblDescription = new javax.swing.JLabel();
        lblMaxParticipants = new javax.swing.JLabel();
        chkRequiresSignup = new javax.swing.JCheckBox();
        spnMaxParticipants = new javax.swing.JSpinner();
        btnSave = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        pnlDay = new javax.swing.JPanel();
        lblDay = new javax.swing.JLabel();
        radioFriday = new javax.swing.JRadioButton();
        radioSaturday = new javax.swing.JRadioButton();
        radioSunday = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        lblStartTime = new javax.swing.JLabel();
        txtTimeStart = new javax.swing.JTextField();
        lblDuration = new javax.swing.JLabel();
        txtDuration = new javax.swing.JTextField();
        cbRooms = new javax.swing.JComboBox();
        lblRoom = new javax.swing.JLabel();
        btnOpenSignup = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblActivities = new javax.swing.JTable();
        lblLoading = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuNews = new javax.swing.JMenu();
        menuNewsAdmin = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Aktiviteter");

        lblName.setText("Navn");

        txtDescription.setColumns(20);
        txtDescription.setRows(5);
        jScrollPane2.setViewportView(txtDescription);

        lblDescription.setText("Beskrivelse");

        lblMaxParticipants.setText("Maksimale antal deltagere");

        chkRequiresSignup.setText("Kræver tilmelding");
        chkRequiresSignup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkRequiresSignupActionPerformed(evt);
            }
        });

        btnSave.setText("Gem aktivitet");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCreate.setText("Opret aktivitet");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnDelete.setText("Slet aktivitet");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        lblDay.setText("Dag");

        btnGrpDay.add(radioFriday);
        radioFriday.setText("Fredag");

        btnGrpDay.add(radioSaturday);
        radioSaturday.setText("Lørdag");

        btnGrpDay.add(radioSunday);
        radioSunday.setText("Søndag");

        javax.swing.GroupLayout pnlDayLayout = new javax.swing.GroupLayout(pnlDay);
        pnlDay.setLayout(pnlDayLayout);
        pnlDayLayout.setHorizontalGroup(
            pnlDayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDayLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioSunday)
                    .addComponent(radioSaturday)
                    .addComponent(radioFriday)
                    .addComponent(lblDay))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlDayLayout.setVerticalGroup(
            pnlDayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDayLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDay)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioFriday)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSaturday)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSunday)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblStartTime.setText("Tid");

        lblDuration.setText("Varighed");

        lblRoom.setText("Lokale");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtTimeStart, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                    .addComponent(lblStartTime)
                    .addComponent(lblDuration)
                    .addComponent(txtDuration))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRoom)
                    .addComponent(cbRooms, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblRoom)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbRooms, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblStartTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTimeStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDuration)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlFormLayout = new javax.swing.GroupLayout(pnlForm);
        pnlForm.setLayout(pnlFormLayout);
        pnlFormLayout.setHorizontalGroup(
            pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFormLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtName)
                    .addComponent(jScrollPane2)
                    .addGroup(pnlFormLayout.createSequentialGroup()
                        .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chkRequiresSignup)
                            .addComponent(lblMaxParticipants)
                            .addComponent(lblName)
                            .addComponent(lblDescription)
                            .addComponent(spnMaxParticipants, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlFormLayout.createSequentialGroup()
                                .addComponent(btnDelete)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCreate)))
                        .addGap(0, 1, Short.MAX_VALUE))
                    .addGroup(pnlFormLayout.createSequentialGroup()
                        .addComponent(pnlDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlFormLayout.setVerticalGroup(
            pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFormLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblDescription)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chkRequiresSignup)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblMaxParticipants)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spnMaxParticipants, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnCreate)
                    .addComponent(btnDelete))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnOpenSignup.setText("Se tilmeldinger");
        btnOpenSignup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenSignupActionPerformed(evt);
            }
        });

        tblActivities.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblActivities.getTableHeader().setReorderingAllowed(false);
        tblActivities.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblActivitiesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblActivities);

        lblLoading.setText("Indlæser..");

        menuNews.setText("Nyheder");

        menuNewsAdmin.setText("Administrer nyheder");
        menuNewsAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNewsAdminActionPerformed(evt);
            }
        });
        menuNews.add(menuNewsAdmin);

        jMenuBar1.add(menuNews);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnOpenSignup)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 301, Short.MAX_VALUE)
                        .addComponent(lblLoading))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlForm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnOpenSignup)
                            .addComponent(lblLoading)))
                    .addComponent(pnlForm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chkRequiresSignupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkRequiresSignupActionPerformed
        onRequiresSignupChange();
    }//GEN-LAST:event_chkRequiresSignupActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        boolean valid = true;

        Activity act = selectedActivity;
        
        String name = txtName.getText();
        String description = txtDescription.getText();
        boolean requiresSignup = chkRequiresSignup.isSelected();
        Room selectedRoom = getSelectedRoom();

        int duration = 0;

        try {
            duration = getDuration();
        } catch (NumberFormatException ex) {
            valid = false;
        }

        int maxParticipants = getMaxParticipants();

        Date startDateTime = null;
        try {
            startDateTime = getStartDateTime();
        } catch (Exception e) {
            valid = false;
        }

        if (valid) {
            controller.saveActivity(selectedActivity, name, description, startDateTime, duration, requiresSignup, maxParticipants, selectedRoom, event);
            tableModel.fireTableDataChanged();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        boolean valid = true;

        String name = txtName.getText();
        String description = txtDescription.getText();
        boolean requiresSignup = chkRequiresSignup.isSelected();
        Room selectedRoom = getSelectedRoom();
        int maxParticipants = getMaxParticipants();
        
        int duration = 0;
        try {
            duration = getDuration();
        } catch (NumberFormatException ex) {
            valid = false;
        }

        Date startDateTime = null;
        try {
            startDateTime = getStartDateTime();
        } catch (Exception e) {
            valid = false;
        }

        if (valid) {
            Activity created = controller.createActivity(name, description, startDateTime, duration, requiresSignup, maxParticipants, selectedRoom, event);
            tableModel.addItem(created);
            tableModel.fireTableDataChanged();
            resetInput();
        }

    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

        if (selectedActivity instanceof Activity) {
            int index = tblActivities.getSelectedRow();
            controller.deleteActivity(selectedActivity);
            tableModel.removeItem(selectedActivity);
            tableModel.fireTableDataChanged();

            if (index > 0) {
                index--;
                tblActivities.setRowSelectionInterval(index, index);
            }

            unselectActivity();
        }

    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnOpenSignupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenSignupActionPerformed
        if (selectedActivity.getRequiresSignup()) {
            JFrame frame = new SignupView(selectedActivity);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }
    }//GEN-LAST:event_btnOpenSignupActionPerformed

    private void tblActivitiesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblActivitiesMouseClicked
        if (evt.getClickCount() == 2) {
            btnOpenSignupActionPerformed(null);
        }
    }//GEN-LAST:event_tblActivitiesMouseClicked

    private void menuNewsAdminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNewsAdminActionPerformed
        JFrame frame = new NewsAdmin();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }//GEN-LAST:event_menuNewsAdminActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.ButtonGroup btnGrpDay;
    private javax.swing.JButton btnOpenSignup;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbRooms;
    private javax.swing.JCheckBox chkRequiresSignup;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDay;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblDuration;
    private javax.swing.JLabel lblLoading;
    private javax.swing.JLabel lblMaxParticipants;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblRoom;
    private javax.swing.JLabel lblStartTime;
    private javax.swing.JMenu menuNews;
    private javax.swing.JMenuItem menuNewsAdmin;
    private javax.swing.JPanel pnlDay;
    private javax.swing.JPanel pnlForm;
    private javax.swing.JRadioButton radioFriday;
    private javax.swing.JRadioButton radioSaturday;
    private javax.swing.JRadioButton radioSunday;
    private javax.swing.JSpinner spnMaxParticipants;
    private javax.swing.JTable tblActivities;
    private javax.swing.JTextArea txtDescription;
    private javax.swing.JTextField txtDuration;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtTimeStart;
    // End of variables declaration//GEN-END:variables
}
