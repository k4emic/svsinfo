package net.mennia.svsinfo.admin.view;

import java.awt.print.PrinterException;
import java.text.MessageFormat;
import net.mennia.svsinfo.admin.controller.SignupController;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Signup;
import net.mennia.svsinfo.model.TeamGenerator;
import net.mennia.svsinfo.admin.view.tablemodel.SignupTableModel;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class SignupView extends javax.swing.JFrame {

    private SignupTableModel tableModel;
    private SignupController controller;
    private Activity activity;

    SignupView(Activity selectedActivity) {
        initComponents();

        activity = selectedActivity;
        controller = new SignupController();

        setTitle(String.format("Tilmeldinger - %s", activity.getName()));

        loadSignups(); // controls are initialized here
    }

    private void initControls() {

        int signups = getSignupCount();

        if (signups == 0) {
            this.remove(pnlControls);
        } else {
            int value = Math.min(activity.getMaxParticipants(), signups);
            int min = 1;
            int max = getSignupCount();

            SpinnerNumberModel participantModel = new SpinnerNumberModel(value, min, max, 1);
            spnMaxParticipants.setModel(participantModel);

            value = 1;
            min = 1;
            max = signups;

            SpinnerNumberModel teamSizeModel = new SpinnerNumberModel(value, min, max, 1);
            spnTeamsize.setModel(teamSizeModel);

            ChangeListener listener = new ChangeListener() {

                @Override
                public void stateChanged(ChangeEvent e) {
                    spinnerValuesChanged();
                }
            };

            spnMaxParticipants.addChangeListener(listener);
            spnTeamsize.addChangeListener(listener);
            spinnerValuesChanged();
        }

    }

    private void spinnerValuesChanged() {
        int participants = getMaxParticipants();
        int teamSize = getTeamSize();
        int noTeams = TeamGenerator.getNoOfTeams(participants, teamSize);
        int inReserve = getNoInReserve();

        lblNoOfTeams.setText(String.format("%d hold", noTeams));
        lblInReserve.setText(String.format("%d i reserve", inReserve));
    }

    private int getNoInReserve() {
        int participants = getMaxParticipants();
        int signups = getSignupCount();

        return signups - participants;
    }

    private void loadSignups() {

        List<Signup> signups = controller.getSignups(activity);
        initTable(signups);

        String participantCount = String.format("%d tilmeldinger", signups.size());
        lblParticipantCount.setText((participantCount));

        initControls(); // reinitialize controls

    }

    private int getMaxParticipants() {
        return (Integer) spnMaxParticipants.getValue();
    }

    private int getTeamSize() {
        return (Integer) spnTeamsize.getValue();
    }

    private int getSignupCount() {
        return tableModel.getRowCount();
    }

    private void initTable(List<Signup> signups) {
        tableModel = new SignupTableModel(signups);
        tblParticipants.setModel(tableModel);
    }

    /**
     * Checks for any invalid input parameters and notifies the user of any problems
     * 
     * @return false if any input fails validation
     */
    private boolean validateInput() {
        boolean pass = true;
        // teamsize > participants
        if(getTeamSize() > getMaxParticipants()) {
            pass = false;
            JOptionPane.showMessageDialog(this, "Holdstørrelsen må ikke være større end antallet af deltagere.");
        }

        if(pass == true) {
            // odd number in teams
            int teamSize = getTeamSize();
            int participants = getMaxParticipants();
            int leftover = participants % teamSize;
            boolean hasEqualTeams = leftover == 0;

            if(!hasEqualTeams) {
                String message = String.format("Sammensætningen vil medføre at et/flere hold vil være i undertal (%d på holdet). "
                        + "\n Er du sikker på at du vil fortsætte?", leftover);
                String title = "";
                int optionType = JOptionPane.YES_NO_OPTION;
                int messageType = JOptionPane.QUESTION_MESSAGE;
                int choice = JOptionPane.showConfirmDialog(this, message, title, optionType, messageType);
                if(choice != JOptionPane.YES_OPTION) {
                    pass = false;
                }

            }

        }
        
        return pass;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblParticipants = new javax.swing.JTable();
        pnlControls = new javax.swing.JPanel();
        btnShowTeams = new javax.swing.JButton();
        btnDeleteSignup = new javax.swing.JButton();
        pnlSpinnerControls = new javax.swing.JPanel();
        lblParticipants = new javax.swing.JLabel();
        lblTeamsize = new javax.swing.JLabel();
        spnTeamsize = new javax.swing.JSpinner();
        spnMaxParticipants = new javax.swing.JSpinner();
        lblNoOfTeams = new javax.swing.JLabel();
        lblInReserve = new javax.swing.JLabel();
        btnPrint = new javax.swing.JButton();
        lblParticipantCount = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tblParticipants.setAutoCreateRowSorter(true);
        tblParticipants.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblParticipants.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblParticipants.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblParticipants);
        tblParticipants.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        btnShowTeams.setText("Lav holdliste");
        btnShowTeams.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowTeamsActionPerformed(evt);
            }
        });

        btnDeleteSignup.setText("Slet tilmelding");
        btnDeleteSignup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteSignupActionPerformed(evt);
            }
        });

        lblParticipants.setText("Deltagere");

        lblTeamsize.setText("Holdstørrelse");

        lblNoOfTeams.setText("hold");

        lblInReserve.setText("reserve");

        javax.swing.GroupLayout pnlSpinnerControlsLayout = new javax.swing.GroupLayout(pnlSpinnerControls);
        pnlSpinnerControls.setLayout(pnlSpinnerControlsLayout);
        pnlSpinnerControlsLayout.setHorizontalGroup(
            pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpinnerControlsLayout.createSequentialGroup()
                .addGroup(pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTeamsize)
                    .addComponent(lblParticipants))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spnTeamsize, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spnMaxParticipants, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(pnlSpinnerControlsLayout.createSequentialGroup()
                .addGroup(pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNoOfTeams)
                    .addComponent(lblInReserve))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlSpinnerControlsLayout.setVerticalGroup(
            pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpinnerControlsLayout.createSequentialGroup()
                .addGroup(pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblParticipants)
                    .addComponent(spnMaxParticipants, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlSpinnerControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTeamsize)
                    .addComponent(spnTeamsize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblNoOfTeams)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblInReserve))
        );

        btnPrint.setText("Udskriv");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        lblParticipantCount.setText("Antal tilmeldinger");

        javax.swing.GroupLayout pnlControlsLayout = new javax.swing.GroupLayout(pnlControls);
        pnlControls.setLayout(pnlControlsLayout);
        pnlControlsLayout.setHorizontalGroup(
            pnlControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnShowTeams, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDeleteSignup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlSpinnerControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlControlsLayout.createSequentialGroup()
                        .addComponent(lblParticipantCount)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnPrint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlControlsLayout.setVerticalGroup(
            pnlControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblParticipantCount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlSpinnerControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnShowTeams)
                .addGap(18, 18, 18)
                .addComponent(btnPrint)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeleteSignup)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addGap(18, 18, 18)
                .addComponent(pnlControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnShowTeamsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowTeamsActionPerformed

        boolean valid = validateInput();

        if (valid) {
            int teamsize = getTeamSize();
            int maxParticipants = getMaxParticipants();

            TeamView view = new TeamView(activity, teamsize, maxParticipants);
            view.setLocationRelativeTo(this);
            view.setVisible(true);
        }

    }//GEN-LAST:event_btnShowTeamsActionPerformed

    private void btnDeleteSignupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteSignupActionPerformed

        int row = tblParticipants.getSelectedRow();
        if (row >= 0) {

            String message = "Er du sikker på at du vil slette tilmeldingen?";
            String title = "Slet tilmelding";
            int optionType = JOptionPane.YES_NO_OPTION;
            int messageType = JOptionPane.QUESTION_MESSAGE;
            int result = JOptionPane.showConfirmDialog(this, message, title, optionType, messageType);

            if (result == JOptionPane.YES_OPTION) {
                try {
                    int modelRow = tblParticipants.convertRowIndexToModel(row);
                    Signup signup = tableModel.getItemAt(modelRow);
                    controller.deleteSignup(signup);
                    loadSignups();

                } catch (IndexOutOfBoundsException e) {
                }
            }

        }


    }//GEN-LAST:event_btnDeleteSignupActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed

        MessageFormat header = new MessageFormat(String.format("Deltagerliste: %s", activity.getName()));
        MessageFormat footer = new MessageFormat("- {0} -");
        try {
            tblParticipants.print(JTable.PrintMode.FIT_WIDTH, header, footer);

        } catch (PrinterException ex) {
            Logger.getLogger(TeamView.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(
                    this, String.format("Der opstod en fejl ved print: %s", ex.getMessage()), null, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteSignup;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnShowTeams;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblInReserve;
    private javax.swing.JLabel lblNoOfTeams;
    private javax.swing.JLabel lblParticipantCount;
    private javax.swing.JLabel lblParticipants;
    private javax.swing.JLabel lblTeamsize;
    private javax.swing.JPanel pnlControls;
    private javax.swing.JPanel pnlSpinnerControls;
    private javax.swing.JSpinner spnMaxParticipants;
    private javax.swing.JSpinner spnTeamsize;
    private javax.swing.JTable tblParticipants;
    // End of variables declaration//GEN-END:variables
}
