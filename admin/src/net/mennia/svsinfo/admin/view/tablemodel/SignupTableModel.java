package net.mennia.svsinfo.admin.view.tablemodel;

import java.util.Collection;
import net.mennia.svsinfo.model.Signup;
import net.mennia.svsinfo.view.tablemodel.AbstractItemTableModel;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class SignupTableModel extends AbstractItemTableModel<Signup> {

    private static final int COL_NAME = 0;
    private static final int COL_USERNAME = 1;
    private static final int COL_GENDER = 2;

    public SignupTableModel() {
        super();
    }

    public SignupTableModel(Collection<Signup> items) {
        super(items);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Signup s = getItemAt(rowIndex);

        Object value = null;

        switch (columnIndex) {
            case COL_NAME:
                value = s.getGuest().getName();
                break;
            case COL_USERNAME:
                value = s.getGuest().getUsername();
                break;

            case COL_GENDER:
                value = s.getGuest().getGender();
                break;
        }

        return value;

    }

    @Override
    public Class getColumnClass(int columnIndex) {
        Class cls = Object.class;

        switch (columnIndex) {
            case COL_NAME:
            case COL_USERNAME:
            case COL_GENDER:
                cls = String.class;
                break;
        }

        return cls;
    }

    @Override
    public String getColumnName(int columnIndex) {
        
        String name = "";
        
        switch (columnIndex) {
            case COL_NAME:
                name = "Navn";
                break;
            case COL_USERNAME:
                name = "Brugernavn";
                break;

            case COL_GENDER:
                name = "Køn";
                break;
        }
        
        return name;
    }
    
    @Override
    public int getColumnCount() {
        return 3;
    }
}
