package net.mennia.svsinfo.admin.view.tablemodel;

import java.util.Date;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.NewsItem;
import net.mennia.svsinfo.view.tablemodel.AbstractItemTableModel;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class NewsTableModel extends AbstractItemTableModel<NewsItem> {

    public static enum Column {

        TITLE("Titel", String.class),
        CONTENT("Indhold", String.class),
        CREATED("Oprettet", Date.class),
        ACTIVITY("Aktivitet", String.class);
        private final Class<?> type;
        private final String name;

        private Column(String name, Class<?> type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public String toString() {
            return name;
        }

        public Class<?> type() {
            return type;
        }
    }

    private Column getColumn(int columnIndex) {
        return Column.values()[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return Column.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object value = null;
        NewsItem item = getItemAt(rowIndex);
        Column col = getColumn(columnIndex);

        switch (col) {
            case ACTIVITY:
                Activity activity = item.getActivity();
                if(activity != null) {
                    value = activity.getName();
                }
                break;
            case CONTENT:
                value = item.getContent();
                break;
            case TITLE:
                value = item.getTitle();
                break;
            case CREATED:
                value = item.getCreated();
                break;
        }

        return value;
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return getColumn(columnIndex).type();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return getColumn(columnIndex).toString();
    }
}
