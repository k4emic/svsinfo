package net.mennia.svsinfo.admin.view.tablemodel;

import java.util.ArrayList;
import java.util.List;
import net.mennia.svsinfo.model.Team;
import net.mennia.svsinfo.model.TeamMember;
import net.mennia.svsinfo.view.tablemodel.AbstractItemTableModel;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class TeamMemberTableModel extends AbstractItemTableModel<TeamMember> {

    static enum Column {
        TEAM,
        NAME,
        USERNAME,
        GENDER,
    }

    public TeamMemberTableModel(List<Team> teams) {

        List<TeamMember> members = new ArrayList<TeamMember>();
        for (Team team : teams) {
            members.addAll(team.getMembers());
        }

        setItems(members);
    }


    @Override
    public int getColumnCount() {
        return Column.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TeamMember member = getItemAt(rowIndex);
        Object val = null;
        Column col = getColumn(columnIndex);
        switch(col) {
            case GENDER:
                val = member.getGuest().getGender();
                break;
            case NAME:
                val = member.getGuest().getName();
                break;
            case USERNAME:
                val = member.getGuest().getUsername();
                break;
            case TEAM:
                val = member.getTeam().getName();
                break;
        }
        
        return val;
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return String.class;
    }

    private Column getColumn(int columnIndex) {
        return Column.values()[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {

        Column col = getColumn(columnIndex);
        
        String name = "";
        switch(col) {
            case GENDER:
                name = "Køn";
                break;
            case NAME:
                name = "Navn";
                break;
            case USERNAME:
                name = "Brugernavn";
                break;
            case TEAM:
                name = "Hold";
                break;
        }

        return name;
    }
}
