package net.mennia.svsinfo.admin.view.tablemodel;

import java.text.SimpleDateFormat;
import java.util.List;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.view.tablemodel.AbstractActivityTableModel;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class AdminActivityTableModel extends AbstractActivityTableModel {
    
    private static final int COL_NAME = 0;
    private static final int COL_START = 1;
    private static final int COL_END = 2;
    private static final int COL_MAX_PARTICIPANTS = 3;
    private static final int COL_REQUIRES_SIGNUP = 4;
    private static final int COL_ROOM_NAME = 5;
    
    public AdminActivityTableModel(List<Activity> activities) {
        super(activities);
    }

    public AdminActivityTableModel() {
        super();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        Activity act = getItemAt(rowIndex);
        
        Object value = null;
        
        switch(columnIndex) {
            case COL_NAME:
                value = act.getName();
                break;
            case COL_START:
                String start = new SimpleDateFormat("E HH:mm").format(act.getTimeOfStart());
                value = start;
                break;
            case COL_END:
                String end = new SimpleDateFormat("E HH:mm").format(act.getTimeOfEnd());
                value = end;
                break;
            case COL_MAX_PARTICIPANTS:
                value = act.getMaxParticipants();
                break;
            case COL_REQUIRES_SIGNUP:
                value = act.getRequiresSignup();
                break;
            case COL_ROOM_NAME:
                value = act.getRoom().getName();
                break;
        }
        
        return value;
        
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
        
        Class cl;
        
        switch(columnIndex) {
            case COL_NAME:
            case COL_ROOM_NAME:
            case COL_START:
            case COL_END:
                cl = String.class;
                break;
            
            case COL_MAX_PARTICIPANTS:
                cl = Integer.class;
                break;
                
            case COL_REQUIRES_SIGNUP:
                cl = Boolean.class;
                break;
                
            default:
                cl = Object.class;
                break;
        }
        
        return cl;
        
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        
        String name = "";
        
        switch(columnIndex) {
            case COL_NAME:
                name = "Navn";
                break;
            case COL_START:
                name = "Start";
                break;
            case COL_END:
                name = "Slut";
                break;
            case COL_MAX_PARTICIPANTS:
                name = "Max deltagere";
                break;
            case COL_REQUIRES_SIGNUP:
                name = "Tilmelding";
                break;
            case COL_ROOM_NAME:
                name = "Lokale";
                break;
        }
        
        return name;
    }
    
}
