package net.mennia.svsinfo.admin.controller;

import java.util.List;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Signup;
import net.mennia.svsinfo.model.Team;
import net.mennia.svsinfo.model.TeamGenerator;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class TeamController {

    public TeamController() {
        
    }

    public List<Team> generateTeams(Activity activity, int teamsize, int maxParticipants) {
        TeamGenerator gen = new TeamGenerator();

        SignupController signupCont = new SignupController();
        List<Signup> signups = signupCont.getSignups(activity);

        return gen.generateTeams(signups, teamsize, maxParticipants);
    }



}
