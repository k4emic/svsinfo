package net.mennia.svsinfo.admin.controller;

import java.util.List;
import net.mennia.svsinfo.dataaccess.SignupDAO;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Guest;
import net.mennia.svsinfo.model.Signup;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class SignupController {

    SignupDAO dao;

    public SignupController() {
        dao = new SignupDAO();
    }

    public List<Guest> getActivityParticipants(Activity activity) {        
        return dao.getActivityParticipants(activity);
    }

    public List<Signup> getSignups(Activity activity) {
        List<Signup> signups = dao.getActivitySignups(activity);

        return signups;

    }

    public void deleteSignup(Signup signup) {
        dao.delete(signup);
    }
    
}
