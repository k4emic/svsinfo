package net.mennia.svsinfo.admin.controller;

import net.mennia.svsinfo.dataaccess.EventDAO;
import net.mennia.svsinfo.model.Event;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class EventController {

    private static Event currentEvent;

    public static Event getCurrentEvent() {
        if(currentEvent == null) {
            EventDAO dao = new EventDAO();
            currentEvent = dao.getLatestEvent();
        }

        return currentEvent;
    }
    
    public Event getLatestEvent() {
        EventDAO dao = new EventDAO();
        
        return dao.getLatestEvent();
    }
    
}
