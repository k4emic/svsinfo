package net.mennia.svsinfo.admin.controller;

import java.util.Date;
import java.util.List;
import net.mennia.svsinfo.dataaccess.ActivityDAO;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Event;
import net.mennia.svsinfo.model.Room;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityController {

    public List<Activity> getActivitiesForEvent(Event event) {

        List<Activity> activities = new ActivityDAO().getActivitiesForEvent(event);

        return activities;

    }

    public List<Activity> getActivitiesForCurrentEvent() {
        Event current = EventController.getCurrentEvent();
        return getActivitiesForEvent(current);
    }

    public void deleteActivity(Activity selectedActivity) {
        ActivityDAO dao = new ActivityDAO();
        dao.delete(selectedActivity);
    }

    public Activity createActivity(String name, String description, Date startTime, int duration, boolean requiresSignup, int maxParticipants, Room room, Event event) {

        Activity act = new Activity();
        act.setName(name);
        act.setDescription(description);
        act.setTimeOfStart(startTime);
        act.setDuration(duration);
        act.setRequiresSignup(requiresSignup);
        act.setEvent(event);
        act.setRoom(room);

        if(act.getRequiresSignup()) {
            act.setMaxParticipants(maxParticipants);
        }

        ActivityDAO dao = new ActivityDAO();
        dao.insert(act);
        return act;
    }

    private void saveActivity(Activity activity) {
        ActivityDAO dao = new ActivityDAO();
        dao.update(activity);
    }

    public void saveActivity(Activity original, String name, String description, Date startTime, int duration, boolean requiresSignup, int maxParticipants, Room room, Event event) {
        populateActivity(original, name, description, startTime, duration, requiresSignup, maxParticipants, room, event);
        saveActivity(original);
    }

    private void populateActivity(Activity activity, String name, String description, Date startTime, int duration, boolean requiresSignup, int maxParticipants, Room room, Event event) {
        activity.setName(name);
        activity.setDescription(description);
        activity.setTimeOfStart(startTime);
        activity.setDuration(duration);
        activity.setRequiresSignup(requiresSignup);
        activity.setEvent(event);
        activity.setRoom(room);

        if (activity.getRequiresSignup()) {
            activity.setMaxParticipants(maxParticipants);
        }
    }
}
