package net.mennia.svsinfo.admin.controller;

import java.util.List;
import net.mennia.svsinfo.dataaccess.RoomDAO;
import net.mennia.svsinfo.model.Event;
import net.mennia.svsinfo.model.Room;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class RoomController {

    public List<Room> getRoomsForEvent(Event event) {
        RoomDAO dao = new RoomDAO();
        
        return dao.getRoomsForEvent(event);
    }
    
    
    
}
