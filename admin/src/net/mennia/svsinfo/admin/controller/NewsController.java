package net.mennia.svsinfo.admin.controller;

import java.util.List;
import net.mennia.svsinfo.dataaccess.NewsDAO;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.NewsItem;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class NewsController {

    private NewsDAO dao;

    public NewsController() {
        dao = new NewsDAO();
    }

    public void create(String title, String content, Activity activity) {

        NewsItem item = new NewsItem();
        item.setTitle(title);
        item.setContent(content);
        item.setActivity(activity);
        item.setEvent(EventController.getCurrentEvent());

        dao.insert(item);

    }

    public List<NewsItem> getItems() {
        return dao.findAll(EventController.getCurrentEvent());
    }

    public void delete(NewsItem selectedItem) {
        dao.delete(selectedItem);
    }

    public void update(NewsItem item, String title, String content, Activity activity) {
        item.setTitle(title);
        item.setContent(content);
        item.setActivity(activity);
        dao.update(item);
    }
}
