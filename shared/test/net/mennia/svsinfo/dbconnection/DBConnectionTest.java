package net.mennia.svsinfo.dbconnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class DBConnectionTest {

    public DBConnectionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of openConnection method, of class DBConnection.
     */
    @Test
    public void testOpenCloseConnection() {
        DBConnection conn = DBConnection.getInstance();
        
        assertFalse(conn.isOpen());
        try {
            conn.open();
        } catch (SQLException ex) {
            fail(ex.getMessage());
        }
        
        assertTrue(conn.isOpen());
        
        conn.close();
        assertFalse(conn.isOpen());
    }
    
    @Test
    public void testExecuteSelect() {
        DBConnection connection = DBConnection.getInstance();
        try {
            connection.open();
        } catch (SQLException ex) {
            fail(ex.getMessage());
        }
        
        String sql = "SELECT * FROM Event WHERE id = ?";
        Object[] args = new Object[] {1};
        
        ResultSet rs = connection.executeSelect(sql, args);
        
        try {
            
            int i = 0;
            while(rs.next()) {
                i++;
            }

            assertSame(1, i);

            rs.first();

            assertEquals(1, rs.getInt("id"));
            assertEquals("SVScon 2012", rs.getString("name"));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date startTime = sdf.parse("2012-01-13 16:00:00");
                assertEquals(startTime, rs.getTimestamp("start_time"));
            } catch (ParseException ex) {
                fail("Could not parse date.");
            }

            Date endTime;
            try {
                endTime = sdf.parse("2012-01-15 16:30:00");
                assertEquals(endTime, rs.getTimestamp("end_time"));
            } catch (ParseException ex) {
                fail("Could not parse date.");
            }
            
        } catch(SQLException e) {
            fail(e.getMessage());
        }
        
        connection.close();
        
    }
    
    @Test
    public void testExecuteInsert() {
        try {
            DBConnection.getInstance().open();
        } catch (SQLException ex) {
            fail(ex.getMessage());
        }
        
        // execute insert query
        String sql = "INSERT INTO event (name, start_time, end_time, location_id) VALUES (?, ?, ?, ?)";
        Object[] args = new Object[] {"test", new Date(), new Date(), 1};
        try {
            DBConnection.getInstance().beginTransaction();
        } catch (SQLException ex) {
            fail("Caught SQLException when starting transaction");
        }
        
        int id = DBConnection.getInstance().executeInsert(sql, args);
        
        // check that generated ID was set
        assertNotNull(id);
        
        // test that the row exists in the database
        sql = "SELECT * FROM event WHERE id = ?";
        args = new Object[] {id};
        ResultSet rs = DBConnection.getInstance().executeSelect(sql, args);
        
        int i = 0;
        try {
            while(rs.next()) {
                i++;
            }
        } catch(SQLException e) {
            fail(e.getMessage());
        }
        
        assertEquals(1, i);
        
        try {
            DBConnection.getInstance().rollback();
        } catch (SQLException ex) {
            fail("Caught SQLException when rolling back transaction");
        }
        
        DBConnection.getInstance().close();
        
    }
    
}
