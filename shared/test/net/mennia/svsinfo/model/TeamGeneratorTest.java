package net.mennia.svsinfo.model;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author k4emic
 */
public class TeamGeneratorTest {

    private TeamGenerator generator;

    public TeamGeneratorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        generator = new TeamGenerator();
    }

    @After
    public void tearDown() {
    }

    private List<Signup> createSignups(int signupCount) {
        Activity activity = new Activity();

        for (int i = 0; i < signupCount; i++) {
            Guest guest = new Guest();
            guest.setName("guest_" + i);
            guest.setId(i);
            activity.addGuest(guest);
        }

        return activity.getSignups();
    }

    /**
     * Test of generateTeams method, of class TeamGenerator.
     */
    @Test
    public void testGenerateTeams() {
        
        
        List<Team> teams;

        List<Signup> signups = createSignups(40);
        int teamSize = 6;
        int maxParticipants = 30;

        teams = generator.generateTeams(signups, teamSize, maxParticipants);

        Team reserveTeam = teams.get(5);

        assertEquals("Number of generated teams is correct", 6, teams.size());
        assertEquals("Teamsize matches specification", teamSize, teams.get(0).getGuests().size());
        assertEquals("Priority teams are placed first", Team.TeamType.priority, teams.get(0).getType());
        assertEquals("Reserve team is the last team in the list", Team.TeamType.reserve, reserveTeam.getType());
        assertEquals("Reserve team contains all remaining guests", 10, reserveTeam.getGuests().size());
        assertEquals("Members on the reserve team retains their original ordering (first)", 30, reserveTeam.getGuests().get(0).getId());
        assertEquals("Members on the reserve team retains their original ordering (last)", 39, reserveTeam.getGuests().get(9).getId());

        assertEquals("First team is called '1'", "1", teams.get(0).getName());
        assertEquals("Second team is called '2'", "2", teams.get(1).getName());
        assertEquals("Third team is called '3'", "3", teams.get(2).getName());

        maxParticipants = 200;
        teams = generator.generateTeams(signups, teamSize, maxParticipants);
        assertEquals("The last teams should only be reserve if there are no left over space.", Team.TeamType.priority, teams.get(teams.size() - 1).getType());

        teamSize = 1;
        maxParticipants = 1;
        teams = generator.generateTeams(signups, teamSize, maxParticipants);
        assertEquals("Check for off-by-one errors", 2, teams.size());

    }

    @Test
    public void testGetNoOfTeams() {

        int p = 10;
        int s = 5;

        assertEquals(2, TeamGenerator.getNoOfTeams(p, s));

        p = 9;
        assertEquals(2, TeamGenerator.getNoOfTeams(p, s));

        p = 1;
        s = 40;
        assertEquals(1, TeamGenerator.getNoOfTeams(p, s));

    }
}
