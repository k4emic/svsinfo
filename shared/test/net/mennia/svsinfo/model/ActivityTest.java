/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.mennia.svsinfo.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import net.mennia.svsinfo.model.Activity.SignupState;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityTest {

    public ActivityTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getSignupState method, of class Activity.
     */
    @Test
    public void testGetSignupState() {
        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();

        cal.add(Calendar.MINUTE, -1);
        Date past = cal.getTime();

        Activity pastActivity = new Activity();
        pastActivity.setTimeOfStart(past);
        SignupState state = pastActivity.getSignupState();

        assertEquals(SignupState.CLOSED, state);

        cal.setTime(now);
        cal.add(Calendar.MINUTE, 5);
        cal.add(Calendar.SECOND, 1);
        Date future = cal.getTime();

        Activity futureActivity = new Activity();
        futureActivity.setTimeOfStart(future);
        state = futureActivity.getSignupState();
        assertEquals(SignupState.OPEN, state);
        
    }
}
