package net.mennia.svsinfo.model;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author k4emic
 */
public class TeamTest {

    public TeamTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of Add method, of class Team.
     */
    @Test
    public void testAdd() {
        // test max members
        Team t = new Team(3);
        assertTrue(t.add(new Guest()));
        assertTrue(t.add(new Guest()));
        assertTrue(t.add(new Guest()));
        assertFalse(t.add(new Guest()));
        assertEquals(3, t.getGuests().size());

        t = new Team();
        assertTrue(t.add(new Guest()));
        assertTrue(t.add(new Guest()));
        assertTrue(t.add(new Guest()));
        assertTrue(t.add(new Guest()));
        assertEquals(4, t.getGuests().size());
        
    }
}
