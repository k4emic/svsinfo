package net.mennia.svsinfo.dbtemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import net.mennia.svsinfo.model.Event;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class JDBCTemplateTest{
    
    public JDBCTemplateTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of queryForObjects method, of class JDBCTemplate.
     */
    @Test
    public void testQueryForObjects() {
        System.out.println("queryForObjects");
        String sql = "SELECT * FROM Event WHERE id = ?";
        Object[] args = {1};
        
        RowMapper<Event> rowMapper = new RowMapper<Event>() {

            @Override
            public Event mapRow(ResultSet rs) throws SQLException {
                Event evt = new Event();
                evt.setId(rs.getInt("id"));
                return evt;
            }
            
        };
        
        List<Event> events = JDBCTemplate.queryForObjects(sql, args, rowMapper);
        
        Event first = events.get(0);
        assertEquals(1, first.getId());
    }
    
    @Test
    public void testQueryForObject() {
        
        System.out.println("queryForObjects");
        String sql = "SELECT * FROM Event WHERE id = ?";
        Object[] args = {1};
        
        RowMapper<Event> rowMapper = new RowMapper<Event>() {

            @Override
            public Event mapRow(ResultSet rs) throws SQLException {
                Event evt = new Event();
                evt.setId(rs.getInt("id"));
                return evt;
            }
            
        };
        
        Event event = JDBCTemplate.queryForObject(sql, args, rowMapper);
        
        assertEquals(1, event.getId());
        
    }
}
