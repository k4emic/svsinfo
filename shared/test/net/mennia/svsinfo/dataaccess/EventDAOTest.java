package net.mennia.svsinfo.dataaccess;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import net.mennia.svsinfo.dbconnection.DBConnection;
import net.mennia.svsinfo.model.Event;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class EventDAOTest {

    public EventDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCurrentEvent method, of class EventDAO.
     */
    @Test
    public void testGetCurrentEvent() {
        DBConnection conn = DBConnection.getInstance();
        EventDAO dao = new EventDAO();
        
        Event currentEvent = dao.getLatestEvent();

        assertEquals("SVScon 2012", currentEvent.getName());
        
        try {
            Date startTime = DateFormat.getInstance().parse("13-01-2012 16:00");
            assertEquals(startTime, currentEvent.getStartTime());
        } catch (ParseException ex) {
            fail();
        }
        
        try {
            Date endTime = DateFormat.getInstance().parse("15-01-2012 16:30");
            assertEquals(endTime, currentEvent.getEndTime());
        } catch (ParseException ex) {
            fail();
        }
    }
}
