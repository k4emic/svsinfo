package net.mennia.svsinfo.dataaccess;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Event;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityDAOTest {
    
    public ActivityDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getActivitiesForEvent method, of class ActivityDAO.
     */
    @Test
    public void testGetActivitiesForEvent() {
        Event event = new Event();
        event.setId(1);
        ActivityDAO actDao = new ActivityDAO();
        List<Activity> list = actDao.getActivitiesForEvent(event);
        
        assertEquals(42, list.size());
        
        Activity activity = list.get(1);
        
        assertEquals(2, activity.getId());
        assertEquals("Velkomst", activity.getName());
        assertEquals("En kort velkomst til arrangementet", activity.getDescription());
        try {
            Date startTime = DateFormat.getInstance().parse("13-01-2012 17:00");
            assertEquals(startTime, activity.getTimeOfStart());
        } catch (ParseException ex) {
            fail();
        }
        assertEquals(60, activity.getDuration());
        assertEquals(0, activity.getMaxParticipants());
        assertEquals(false, activity.getRequiresSignup());
        
        assertEquals(4, activity.getRoom().getId());
        assertEquals("Salen", activity.getRoom().getName());
    }
}
