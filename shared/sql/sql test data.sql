USE svsinfo;

START TRANSACTION;

DELETE FROM signup;
DELETE FROM guest;
DELETE FROM activity;
DELETE FROM room;
DELETE FROM event;
DELETE FROM location;


INSERT INTO location (id, name)
    VALUES 
        (1, 'Sportscenter Herning');

INSERT INTO event (id, name, start_time, end_time, location_id)
    VALUES 
        (1, 'SVScon 2012', '2012-01-13 16:00:00', '2012-01-15 16:30:00', 1);

INSERT INTO room (id, name, location_id)
    VALUES
        (1, 'Rødt rum', 1), 
        (2, 'Blåt rum', 1),
        (3, 'Gult rum', 1),
        (4, 'Salen', 1);

INSERT INTO activity (id, name, description, start_time, duration, max_participants, requires_signup, event_id, room_id)
    VALUES 
        (1, 'Dørene åbner', '', '2012-01-13 16:30:00', 0, NULL, 0, 1, 4),
        (2, 'Velkomst', 'En kort velkomst til arrangementet', '2012-01-13 17:00', 60, NULL, 0, 1, 4),
        (3, 'Freestyle', '', '2012-01-13 20:00:00', 60, NULL, 0, 1, 4),
        (4, 'Hvem vil være milionær', '', '2012-01-13 21:00:00', 30, NULL, 0, 1, 4),
        (5, 'Freestyle forsat', '', '2012-01-13 21:30:00', 60, NULL, 0, 1, 4),
        (6, 'Photo hunt', '', '2012-01-13 21:30:00', 90, 15, 1, 1, 1),
        (7, 'Cosplay acts', 'Panel', '2012-01-13 22:00:00', 60, NULL, 0, 1, 2),
        (8, 'Pose-off', '', '2012-01-13 22:30:00', 90, NULL, 0, 1, 4),
        (9, 'Erfarne cosplayere', 'Panel', '2012-01-13 23:00:00', 90, NULL, 0, 1, 2),
        (10, 'Marvel vs Capcom konkurrence', '', '2012-01-13 23:00:00', 90, 30, 1, 1, 1),
        (11, 'Werewolves', '', '2012-01-14 00:00:00', 120, NULL, 0, 1, 4),
        (12, 'Maid RPG', '', '2012-01-14 00:30:00', 60, NULL, 0, 1, 2),
        (13, 'Auktion', '', '2012-01-14 10:00:00', 90, NULL, 0, 1, 4),
        (14, 'Musikquiz', '', '2012-01-14 10:00:00', 60, 15, 1, 1, 2),
        (15, 'Speed meeting', '', '2012-01-14 10:00:00', 90, NULL, 0, 1, 1),
        (16, 'Holdquiz', '', '2012-01-14 11:30:00', 120, 10, 1, 1, 4),
        (17, 'Mario Kart Wii konkurrence', '', '2012-01-14 11:30:00', 120, 20, 1, 1, 1),
        (18, 'Spilquiz', '', '2012-01-14 13:30:00', 120, 10, 1, 1, 4),
        (19, 'Cosplay møde', '', '2012-01-14 13:00:00', 120, NULL, 0, 1, 3),
        (20, 'Frivillige hjælpere', 'Panel', '2012-01-14 13:30:00', 120, NULL, 0, 1, 2),
        (21, 'Japanstudier', 'Foredrag', '2012-01-14 13:30:00', 90, NULL, 0, 1, 1),
        (22, 'Turen går til Japan', 'Foredrag', '2012-01-14 15:00:00', 90, NULL, 0, 1, 1),
        (23, 'Go Fetch!', '', '2012-01-14 15:30:00', 90, NULL, 0, 1, 4),
        (24, 'Ny(t) i miljøet', 'Panel', '2012-01-14 15:30:00', 120, NULL, 0, 1, 2),
        (25, 'Quizløb', '', '2012-01-14 17:00:00', 60, NULL, 0, 1, 4),
        (26, 'Superquiz', '', '2012-01-14 18:00:00', 90, 10, 1, 1, 2),
        (27, 'Kødannelse', '', '2012-01-14 19:30:00', 30, NULL, 0, 1, 3),
        (28, 'SVScosplayshow', '', '2012-01-14 20:00:00', 120, NULL, 0, 1, 3),
        (29, 'Anime Dating Game', '', '2012-01-14 22:00:00', 60, NULL, 0, 1, 3),
        (30, 'Annoncering af vinderne', '', '2012-01-14 23:00:00', 30, NULL, 0, 1, 3),
        (31, 'Gæt og animer', '', '2012-01-14 23:30:00', 90, NULL, 0, 1, 2),
        (32, 'Plushie dødbold', '', '2012-01-15 00:00:00', 120, NULL, 0, 1, 4),
        (33, 'Werewolves', '', '2012-01-15 01:30:00', 120, NULL, 0, 1, 2),
        (34, 'Hobo cosplay', '', '2012-01-15 10:00:00', 90, 30, 1, 1, 4),
        (35, 'Tip og trick til AMV', 'Foredrag', '2012-01-15 10:00:00', 60, NULL, 0, 1, 2),
        (36, 'Yu-Gi-Oh! Tunering', '', '2012-01-15 10:00:00', 120, NULL, 0, 1, 1),
        (37, 'AMV konkurrence', '', '2012-01-15 11:00:00', 120, NULL, 0, 1, 2),
        (38, 'SVS statistik', '', '2012-01-15 11:30:00', 90, NULL, 0, 1, 4),
        (39, 'Last man standing', '', '2012-01-15 13:00:00', 90, NULL, 0, 1, 4),
        (40, 'SVScons fremtid', '', '2012-01-15 13:00:00', 120, NULL, 0, 1, 2),
        (41, 'Afslutnings ceremoni', '', '2012-01-15 14:30:00', 30, NULL, 0, 1, 4),
        (42, 'Connen lukker', '', '2012-01-15 15:00:00', 30, NULL, 0, 1, 4);
		
INSERT INTO guest(id, username, name, gender, birthdate) 
	VALUES 
		(1, 'gæst1', 'Mads', 'M', "1989-06-10"),
		(2, 'gæst1', 'Mads', 'M', "1989-06-10"),
		(3, 'gæst1', 'Mads', 'M', "1989-06-10"),
		(4, 'gæst1', 'Mads', 'M', "1989-06-10"),
		(5, 'gæst1', 'Mads', 'M', "1989-06-10");
		
INSERT INTO signup(guest_id, activity_id, `time`) 
	VALUES
		(1, 16, "2012-01-01 15:00:30"),
		(2, 16, "2012-01-01 15:00:29"),
		(3, 16, "2012-01-01 15:00:28"),
		(4, 16, "2012-01-01 15:00:27"),
		(5, 16, "2012-01-01 15:00:26");
  
COMMIT;
