use svsinfo;

START TRANSACTION;

CREATE TABLE location (
	id     int AUTO_INCREMENT NOT NULL,
	name   varchar(100) NOT NULL,

	PRIMARY KEY (id)
);

CREATE TABLE event (
	id          int AUTO_INCREMENT NOT NULL,
	name        varchar(60) NOT NULL,
	start_time  datetime NOT NULL,
	end_time    datetime NOT NULL,
	location_id int NOT NULL,
	
	PRIMARY KEY(id),
	FOREIGN KEY(location_id) REFERENCES location (id)
);

CREATE TABLE room (
	id            int AUTO_INCREMENT NOT NULL,
	name          varchar(100) NOT NULL,
	location_id   int NOT NULL,
	
	PRIMARY KEY (id),
	FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE
);

CREATE TABLE activity (
	id               int AUTO_INCREMENT NOT NULL,
	name             varchar(50) NOT NULL,
	description      text NOT NULL,
	start_time       datetime NOT NULL,
	duration         int NOT NULL,
	max_participants int,
	requires_signup  bit NOT NULL,
	event_id         int NOT NULL,
	room_id          int NOT NULL,
	
	PRIMARY KEY (id),
	FOREIGN KEY (event_id) REFERENCES event(id),
	FOREIGN KEY (room_id) REFERENCES room(id)
);

CREATE TABLE news_item (
	id          int AUTO_INCREMENT NOT NULL,
	title       varchar(100) NOT NULL,
	content     text NOT NULL,
	`time`      datetime NOT NULL,
	event_id    int NOT NULL,
	activity_id int,
	
	PRIMARY KEY(id),
	FOREIGN KEY (event_id) REFERENCES event(id),
	FOREIGN KEY (activity_id) REFERENCES activity(id)
);

CREATE TABLE guest (
	id        int AUTO_INCREMENT NOT NULL,
	username  varchar(100) NOT NULL,
	name      varchar(100) NOT NULL,
	gender    char(1) NOT NULL,
	birthdate datetime NOT NULL,
	
	PRIMARY KEY (id),
	UNIQUE INDEX `username_UNIQUE` (`username` ASC)
);

CREATE TABLE signup (
	activity_id int NOT NULL,
	guest_id    int NOT NULL,
	`time`      datetime NOT NULL,
	
	PRIMARY KEY (activity_id, guest_id),
	FOREIGN KEY (activity_id) REFERENCES activity (id),
	FOREIGN KEY (guest_id) REFERENCES guest (id)
);

COMMIT;
