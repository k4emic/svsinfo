package net.mennia.svsinfo.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Config {

    public enum Key {
        DBHOST("dbhost"),
        DBUSER("dbuser"),
        DBPASSWORD("dbpass"),
        DBSCHEMA("dbschema"),
        DBDEBUG("dbdebug"),
        INACTIVITYTIMEOUT("inactivityTimeout"),
        PREVENTSHUTDOWN("preventShutdown");

        private String id;
        Key(String id) {
            this.id = id;
        }

        public String id() {
            return id;
        }
    }

    private static Config instance;
    private Properties conf;

    private Config() {
        conf = new Properties();
    }

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }

        return instance;
    }

    /**
     * Loads the file as a properties file
     *
     * @param filename
     */
    public void loadPropertiesFile(File configFile) throws FileNotFoundException, IOException {

        FileReader reader = new FileReader(configFile);
        try {
            conf.load(reader);
        } catch (IOException ex) {
            throw ex;
        } finally {
            reader.close();
        }

    }

    public void loadPropertiesStream(InputStream stream) throws IOException {
        conf.load(stream);
    }

    public String getString(Config.Key key) {
        String val = conf.getProperty(key.id());
        return val;
    }

    public boolean getBool(Config.Key key) {
        String val = getString(key);

        if(val.equals("true") || val.equals("1")) {
            return true;
        }

        return false;
    }

    public int getInt(Config.Key key) {
        return Integer.parseInt(getString(key));
    }
    
}
