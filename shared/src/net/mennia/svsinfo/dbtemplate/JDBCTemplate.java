package net.mennia.svsinfo.dbtemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.mennia.svsinfo.dbconnection.DBConnection;

/**
 * Template class for easier access to data storage and mapping to model objects
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class JDBCTemplate {
    
    /**
     * Queries the database and returns the resulting model objects in a list.
     * 
     * @param <T> Type that the returned list contains
     * @param sql The query to execute
     * @param args Arguments for the query
     * @param mapper The mapper to use
     * @return 
     */
    public static <T> List<T> queryForObjects(String sql, Object[] args, RowMapper<T> mapper) {
        
        DBConnection conn = DBConnection.getInstance();
        ArrayList<T> list = new ArrayList<T>();
        
        try {
            conn.open();
            
            ResultSet rs = conn.executeSelect(sql, args);
            
            while(rs.next()) {
                T object = mapper.mapRow(rs);
                list.add(object);
            }
            
            rs.close();
            
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    /**
     * Similar to queryForObjects, but only fetches one object
     * @param <T>
     * @param sql
     * @param args
     * @param mapper
     * @return 
     */
    public static <T> T queryForObject(String sql, Object[] args, RowMapper<T> mapper) {
        DBConnection conn = DBConnection.getInstance();
        T object = null;
        
        try {
            conn.open();
            
            ResultSet rs = conn.executeSelect(sql, args);
            
            if(rs.next()) {
                object = mapper.mapRow(rs);
            }
            
            rs.close();
            
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return object;
    }
    
    /**
     * Performs the specified query and returns the first column in the first row as an integer
     * @param sql
     * @param args
     * @return 
     */
    public static int queryForInteger(String sql, Object[] args) {
        return queryForObject(sql, args, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet rs) throws SQLException {
                return rs.getInt(1);
            }
        });
    }
    
    /**
     * Wrapper method for executing standard update/delete queries.
     * Returns the number of affected rows.
     * @param sql
     * @param args
     * @return 
     */
    public static int execute(String sql, Object[] args) {
        
        DBConnection conn = DBConnection.getInstance();
        
        int result = 0;
        
        try {
            conn.open();
            result = conn.execute(sql, args);
            conn.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(JDBCTemplate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
        
    }
    
    public static int insert(String sql, Object[] args) {
        
        DBConnection conn = DBConnection.getInstance();
        
        int id = 0;
        
        try {
            conn.open();
            id = conn.executeInsert(sql, args);
            conn.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(JDBCTemplate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
        
    }
    
}
