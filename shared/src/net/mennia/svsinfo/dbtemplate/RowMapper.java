package net.mennia.svsinfo.dbtemplate;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public interface RowMapper<T> {
    
    T mapRow(ResultSet rs) throws SQLException;
    
}
