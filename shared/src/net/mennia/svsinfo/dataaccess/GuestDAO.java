package net.mennia.svsinfo.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.mennia.svsinfo.dbtemplate.JDBCTemplate;
import net.mennia.svsinfo.dbtemplate.RowMapper;
import net.mennia.svsinfo.model.Guest;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class GuestDAO extends GenericDAO<Guest, Integer> {

    @Override
    public Guest findByPK(Integer id) {
        String sql = "SELECT * FROM guest WHERE id = ? LIMIT 1";
        Object[] args = new Object[] {id};
        
        return findOne(sql, args, new FullGuestMapper());
    }

    @Override
    public void insert(Guest guest) {
        
        String sql = "INSERT INTO guest(username, name, gender, birthdate) VALUES(?, ?, ?, ?)";
        
        Object[] args = new Object[] {guest.getUsername(), guest.getName(), 
            guest.getGender(), guest.getBirthdate()};
        
        int id = JDBCTemplate.insert(sql, args);
        
        guest.setId(id);
        
    }

    @Override
    public void update(Guest model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Guest model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Guest findByUsername(String username) {
        String sql = "SELECT * FROM guest g WHERE g.username = ?";
        Object[] args = {username};
        
        return JDBCTemplate.queryForObject(sql, args, new FullGuestMapper());
    }

    private class FullGuestMapper implements RowMapper<Guest> {

        @Override
        public Guest mapRow(ResultSet rs) throws SQLException {
            Guest guest = new Guest();
            
            guest.setId(rs.getInt("id"));
            guest.setUsername(rs.getString("username"));
            guest.setBirthdate(rs.getDate("birthdate"));
            guest.setGender(rs.getString("gender"));
            guest.setName(rs.getString("name"));

            return guest;
        }
        
    }
}
