package net.mennia.svsinfo.dataaccess;

import java.util.List;
import net.mennia.svsinfo.dbtemplate.JDBCTemplate;
import net.mennia.svsinfo.dbtemplate.RowMapper;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public abstract class GenericDAO<T, PK> implements GenericDAOInterface<T, PK> {
    
    protected T findOne(String sql, Object[] args, RowMapper<T> mapper) {
        return JDBCTemplate.queryForObject(sql, args, mapper);
    }
    
    protected List<T> findAll(String sql, Object[] args, RowMapper<T> mapper) {
        return JDBCTemplate.queryForObjects(sql, args, mapper);
    }
    
}
