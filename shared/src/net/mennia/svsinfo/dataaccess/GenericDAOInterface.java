package net.mennia.svsinfo.dataaccess;

/**
 * This interface is mainly used to ensure that all DAOs use the same names for their operations
 * @author Mads Nielsen <k4emic@gmail.com>
 * @param <T> Model datatype (i.e: User, Activity, Signup)
 * @param <PK> Primary key datatype (Usually Integer)
 */
public interface GenericDAOInterface <T, PK> {
    
    /**
     * Searches for a record based on its primary key
     * @param pk
     * @return
     */
    public T findByPK(PK pk);
    
    /**
     * Updates the stored record that represents this object
     * @param model
     * @throws SQLException 
     */
    public void update(T model);
    
    /**
     * Deletes the stored record for this object
     * @param model 
     */
    public void delete(T model);
    
    /**
     * Inserts this object into the storage and sets the models primary key
     * @param model
     * @return 
     */
    public void insert(T model);
    
}
