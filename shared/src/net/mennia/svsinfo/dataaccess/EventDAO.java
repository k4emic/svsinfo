package net.mennia.svsinfo.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.mennia.svsinfo.dbtemplate.RowMapper;
import net.mennia.svsinfo.model.Event;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class EventDAO extends GenericDAO<Event, Integer> {

    public Event getLatestEvent() {
        String sql = "SELECT * FROM event ORDER BY start_time DESC LIMIT 1";
        return findOne(sql, new Object[] {}, new FullEventMapper());
    }
    
    @Override
    public Event findByPK(Integer pk) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(Event model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Event model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void insert(Event model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    private class FullEventMapper implements RowMapper<Event> {

        @Override
        public Event mapRow(ResultSet rs) throws SQLException {
            Event event = new Event();
            
            event.setId(rs.getInt("id"));
            event.setName(rs.getString("name"));
            event.setStartTime(rs.getTimestamp("start_time"));
            event.setEndTime(rs.getTimestamp("end_time"));
            
            return event;
        }
    }
    
}
