package net.mennia.svsinfo.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import net.mennia.svsinfo.dbtemplate.RowMapper;
import net.mennia.svsinfo.model.Event;
import net.mennia.svsinfo.model.Room;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class RoomDAO extends GenericDAO<Room, Integer> {

    private HashMap<Integer, Room> cache;

    public RoomDAO() {
        cache = new HashMap<Integer, Room>();
    }

    @Override
    public Room findByPK(Integer id) {

        Room result;
        if (cache.containsKey(id)) {
            result = cache.get(id);
        } else {
            String sql = "SELECT * FROM room WHERE id = ?";
            Object[] args = new Object[]{id};

            result = findOne(sql, args, new FullRoomMapper());

            cache.put(id, result);
        }

        return result;
    }

    @Override
    public void update(Room model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Room model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void insert(Room model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Room> getRoomsForEvent(Event event) {
        String sql = "SELECT r.* FROM room r "
                + "INNER JOIN location l ON l.id = r.location_id "
                + "INNER JOIN `event` e ON e.location_id = l.id "
                + "WHERE e.id = ?";

        Object[] args = {event.getId()};

        List<Room> rooms = findAll(sql, args, new FullRoomMapper());

        return rooms;
    }

    private class FullRoomMapper implements RowMapper<Room> {

        @Override
        public Room mapRow(ResultSet rs) throws SQLException {
            Room room = new Room();

            room.setName(rs.getString("name"));
            room.setId(rs.getInt("id"));

            cache.put(room.getId(), room);

            return room;
        }
    }
}
