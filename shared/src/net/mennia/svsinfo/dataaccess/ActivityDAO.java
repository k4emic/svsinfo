package net.mennia.svsinfo.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.mennia.svsinfo.dbconnection.DBConnection;
import net.mennia.svsinfo.dbtemplate.JDBCTemplate;
import net.mennia.svsinfo.dbtemplate.RowMapper;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Event;


/**
 * 
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class ActivityDAO implements GenericDAOInterface<Activity, Integer> {
    
    /**
     * Fetches all activities in the storage for a given event. 
     * Results are sorted by their start time attribute.
     * @param event
     * @return 
     */
    public List<Activity> getActivitiesForEvent(Event event) {
        int eventId = event.getId();
        String sql = "SELECT * FROM activity WHERE event_id = ? ORDER BY start_time, name ASC";
        Object[] args = new Object[] {eventId};
        
        return JDBCTemplate.queryForObjects(sql, args, new FullActivityMapper());
    }

    @Override
    public Activity findByPK(Integer pk) {
        String sql = "SELECT * FROM activity WHERE id = ?";
        Object[] args = new Object[] {
            pk
        };

        return JDBCTemplate.queryForObject(sql, args, new FullActivityMapper());
    }

    @Override
    public void update(Activity model) {
        String sql = "UPDATE activity "
                + "SET name = ?, "
                + "description = ?, "
                + "start_time = ?, "
                + "duration = ?, "
                + "max_participants = ?, "
                + "requires_signup = ?, "
                + "event_id = ?, "
                + "room_id = ? "
                + "WHERE id = ?";
        
        Object[] args = {
            model.getName(),
            model.getDescription(),
            model.getTimeOfStart(),
            model.getDuration(),
            model.getMaxParticipants(),
            model.getRequiresSignup(),
            model.getEvent().getId(),
            model.getRoom().getId(),
            model.getId()
        };
        
        JDBCTemplate.execute(sql, args);
    }

    @Override
    public void delete(Activity model) {
        String sql = "DELETE FROM activity WHERE id = ?";
        Object[] args = {model.getId()};
        
        JDBCTemplate.execute(sql, args);
    }

    @Override
    public void insert(Activity model) {
        String sql = "INSERT INTO activity ("
                + "name, description, start_time, duration, max_participants, "
                + "requires_signup, event_id, room_id"
                + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        
        Object[] args = {
            model.getName(),
            model.getDescription(),
            model.getTimeOfStart(),
            model.getDuration(),
            model.getMaxParticipants(),
            model.getRequiresSignup(),
            model.getEvent().getId(),
            model.getRoom().getId()
        };
        
        DBConnection conn = DBConnection.getInstance();
        try {
            conn.open();
            int id = conn.executeInsert(sql, args);
            conn.close();
            
            model.setId(id);
        } catch (SQLException ex) {
            Logger.getLogger(ActivityDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Standard class for mapping a resultset with access to all columns into an activity
     */
    private class FullActivityMapper implements RowMapper<Activity> {

        private RoomDAO roomdao;

        public FullActivityMapper() {
            roomdao = new RoomDAO();
        }

        @Override
        public Activity mapRow(ResultSet rs) throws SQLException {

            Activity activity = new Activity();
                
            activity.setId(rs.getInt("id"));
            activity.setName(rs.getString("name"));
            activity.setDescription(rs.getString("description"));
            activity.setDuration(rs.getInt("duration"));
            activity.setTimeOfStart(rs.getTimestamp("start_time"));
            activity.setRequiresSignup(rs.getBoolean("requires_signup"));
            
            activity.setRoom(roomdao.findByPK(rs.getInt("room_id")));

            int maxParticipants = rs.getInt("max_participants");
            if(!rs.wasNull()) {
                activity.setMaxParticipants(maxParticipants);
            }
            
            return activity;
            
        }
        
    }
    
    
}
