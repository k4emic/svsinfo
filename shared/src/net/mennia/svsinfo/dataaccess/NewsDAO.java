package net.mennia.svsinfo.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import net.mennia.svsinfo.dbtemplate.JDBCTemplate;
import net.mennia.svsinfo.dbtemplate.RowMapper;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Event;
import net.mennia.svsinfo.model.NewsItem;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class NewsDAO extends GenericDAO<NewsItem, Integer> {

    @Override
    public NewsItem findByPK(Integer pk) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(NewsItem model) {
        String sql = "UPDATE news_item SET title = ?, content = ?, activity_id = ? WHERE id = ?";

        Integer activityId = null;
        
        if(model.getActivity() != null) {
            activityId = model.getActivity().getId();
        }

        Object[] args = {
            model.getTitle(),
            model.getContent(),
            activityId,
            model.getId()
        };

        JDBCTemplate.execute(sql, args);
    }

    @Override
    public void delete(NewsItem model) {
        String sql = "DELETE FROM news_item WHERE id = ?";
        Object[] args = {
            model.getId()
        };

        JDBCTemplate.execute(sql, args);
    }

    @Override
    public void insert(NewsItem model) {
        String sql = "INSERT INTO news_item(title, content, event_id, activity_id, `time`) VALUES(?, ?, ?, ?, NOW())";
        
        Integer activityId = null;
        Activity activity = model.getActivity();
        if(activity != null) {
            activityId = activity.getId();
        }

        Object[] args = {
            model.getTitle(),
            model.getContent(),
            model.getEvent().getId(),
            activityId
        };

        JDBCTemplate.insert(sql, args);
    }

    public List<NewsItem> findAll(Event event) {
        String sql = "SELECT * FROM news_item WHERE event_id = ? ORDER BY `time` DESC";
        Object[] args = {
            event.getId()
        };

        return JDBCTemplate.queryForObjects(sql, args, new NewsItemRowMapper(event));
    }

    private class NewsItemRowMapper implements RowMapper<NewsItem> {
        
        private Event event;
        private ActivityDAO actDao;

        public NewsItemRowMapper(Event event) {
            this.event = event;
            actDao = new ActivityDAO();
        }

        @Override
        public NewsItem mapRow(ResultSet rs) throws SQLException {
            NewsItem item = new NewsItem();

            item.setId(rs.getInt("id"));
            item.setTitle(rs.getString("title"));
            item.setContent(rs.getString("content"));
            item.setEvent(this.event);
            item.setCreated(rs.getTimestamp("time"));

            int activityId = rs.getInt("activity_id");
            if(!rs.wasNull()) {
                Activity activity = actDao.findByPK(activityId);
                item.setActivity(activity);
            }

            return item;
        }
        
    }

}
