package net.mennia.svsinfo.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import net.mennia.svsinfo.dbtemplate.JDBCTemplate;
import net.mennia.svsinfo.dbtemplate.RowMapper;
import net.mennia.svsinfo.model.Activity;
import net.mennia.svsinfo.model.Guest;
import net.mennia.svsinfo.model.Signup;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class SignupDAO extends GenericDAO<Signup, Object> {

    public int countSignups(Activity activity) {
        String sql = "SELECT COUNT(*) AS count FROM signup WHERE activity_id = ?";
        Object[] args = new Object[] {activity.getId()};
        
        return JDBCTemplate.queryForInteger(sql, args);
    }

    public void create(Signup signup) {
        
        String sql = "INSERT INTO signup (activity_id, guest_id, time) VALUES(?, ?, ?)";
        
        Object[] args = new Object[] {
            signup.getActivity().getId(),
            signup.getGuest().getId(),
            new Date()
        };
        
        JDBCTemplate.insert(sql, args);
    }

    @Override
    public Signup findByPK(Object pk) {
        throw new UnsupportedOperationException("PK consists of multiple PKs");
    }

    /**
     * Fetches list of guests who has signed up for the specified activity
     * @param activity
     * @return
     */
    public List<Guest> getActivityParticipants(Activity activity) {
        List<Signup> signups = getActivitySignups(activity);
        return Signup.extractGuests(signups);        
    }

    /**
     * Fethces all signups related to the activity
     * @param activity
     * @return
     */
    public List<Signup> getActivitySignups(Activity activity) {
        String sql = "SELECT * FROM signup s "
                + "LEFT JOIN guest g ON s.guest_id = g.id "
                + "WHERE activity_id = ? "
                + "ORDER BY `time` DESC";

        Object[] args = {activity.getId()};

        List<Signup> signups = JDBCTemplate.queryForObjects(sql, args, new SignupGuestMapper(activity));

        return signups;
    }

    @Override
    public void update(Signup model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Signup model) {
        String sql = "DELETE FROM signup WHERE guest_id = ? AND activity_id = ?";

        Object[] args = {
            model.getGuest().getId(),
            model.getActivity().getId()
        };

        JDBCTemplate.execute(sql, args);
    }

    @Override
    public void insert(Signup model) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private class SignupGuestMapper implements RowMapper<Signup> {

        private Activity activity;

        public SignupGuestMapper(Activity activity) {
            this.activity = activity;
        }

        @Override
        public Signup mapRow(ResultSet rs) throws SQLException {

            Guest guest = new Guest();
            guest.setId(rs.getInt("id"));
            guest.setName(rs.getString("name"));
            guest.setUsername(rs.getString("username"));
            guest.setBirthdate(rs.getDate("birthdate"));
            guest.setGender(rs.getString("gender"));

            Signup signup = new Signup(activity, guest);

            return signup;

        }

    }

}
