package net.mennia.svsinfo.dbconnection;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class DBConnection {

    private static DBConnection instance;
    private Connection connection;
    private boolean debug;
    
    private String hostname;
    private String schema;
    private String username;
    private String password;

    private DBConnection() {
        debug = false;
        
        hostname = "";
        schema = "";
        username = "";
        password = "";
    }
    
    /**
     * Fetches the singleton for the database
     */
    public static DBConnection getInstance() {

        if (instance == null) {
            instance = new DBConnection();
        }

        return instance;
    }
    
    public void setDebug(boolean bool) {
        this.debug = bool;
    }
    
    public void setHostname(String hostname) {
         this.hostname = hostname;
    }
    
    public void setSchema(String schema) {
        this.schema = schema;
    }
    
    /**
     * Opens the connection to the database based on current configuration
     */
    public void open() throws SQLException {
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception ex) {
            System.out.println(ex.getCause());
        }
        
        String connectionFormat = "jdbc:mysql://%s/%s?user=%s&password=%s";
        
        String connectionUrl = String.format(connectionFormat, hostname, schema, username, password);
        
        connection = DriverManager.getConnection(connectionUrl);
        
    }

    /**
     * Closes the database connection
     */
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Checks if the connection to the database is open
     * @return true if the connection is open, false otherwise
     */
    public boolean isOpen() {
        
        if(connection != null) {
            try {
               return !connection.isClosed();
            } catch (SQLException ex) {
                return false;
            }
        } else
            return false;
    }
    
    /**
     * Executes a SQL insert command
     * @param sql The sql to use
     * @param args The arguments to use in the query
     * @return The auto-generated ID, null if no ID was generated
     */
    public int executeInsert(String sql, Object[] args) {    
        
        int generatedId = 0;
        
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            bindArgs(pstmt, args);
            logSql(sql, args);
            pstmt.execute();
            ResultSet keys = pstmt.getGeneratedKeys();
            if(keys.next()) {
                generatedId = keys.getInt(1);
            }
            
            pstmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return generatedId;
         
    }
    
    /**
     * Binds all given arguments to the prepared statement
     * @param pstmt
     * @param params
     * @throws SQLException 
     */
    protected void bindArgs(PreparedStatement pstmt, Object[] params) throws SQLException {
        int index = 1;
        for (Object param : params) {
            pstmt.setObject(index, param);
            index++;
        }
        
    }
    
    /**
     * Executes a statement against the database and returns the number of rows affected
     * @param sql
     * @param args
     * @return 
     */
    public int execute(String sql, Object[] args) throws SQLException {
        int result = 0;
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            bindArgs(pstmt, args);
            logSql(sql, args);
            result = pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    /**
     * Alias of execute
     * @param sql
     * @param args
     * @return 
     */
    public int executeDelete(String sql, Object[] args) throws Exception {
        return execute(sql, args);
    }
    
    /**
     * Alias of execute
     * @param sql
     * @param args
     * @return 
     */
    public int executeUpdate(String sql, Object[] args) throws Exception {
        return execute(sql, args);
    }
    
    /**
     * Executes a SQL select query and returns the results as a list with maps
     * @param sql SQL string to use
     * @param args Arguments to bind with the SQL string
     * @return 
     */
    public ResultSet executeSelect(String sql, Object[] args) {
        
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            bindArgs(pstmt, args);
            ResultSet rs = pstmt.executeQuery();
            logSql(sql, args);
            
            return rs;
            
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Same as executeSelect(sql, args), but without arguments
     * @param sql
     * @return 
     */
    public ResultSet executeSelect(String sql) {
        return executeSelect(sql, new Object[] {});
    }
    
    /**
     * Unpacks the current row in the resultset and returns it as a hashmap, 
     * where the column names (from the row) are used as index keys in the hashmap.
     * @param rs
     * @return
     * @throws SQLException 
     */
    private HashMap<String, Object> unpackResultRow(ResultSet rs) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        
        int columns = meta.getColumnCount();
        
        HashMap<String, Object> row = new HashMap<String, Object>();
        String label;
        int type;
        Object value;
        for(int i = 1; i <= columns; i++) {
            label = meta.getColumnLabel(i);
            type = meta.getColumnType(i);
            
            switch(type){                
                default:
                    value = rs.getObject(i);
                    break;
            }
            
            row.put(label, value);
        }
        
        return row;
    }
    
    /**
     * Logs sql queries
     * @param sql
     * @param args 
     */
    private void logSql(String sql, Object[] args) {
        
        if(!debug) {
            return;
        }
        
        String argString = "";
        
        if(args.length > 0) {
            
            argString += " (";
            
            for(Object arg : args) {
                argString += arg + ",";
            }
            
            argString += ")";
            
        }
        
        System.out.println(sql +  argString);
    }

    public void beginTransaction() throws SQLException {
        connection.setAutoCommit(false);
    }
    
    public void commit() throws SQLException {
        connection.commit();
        connection.setAutoCommit(true);
    }
    
    public void rollback() throws SQLException {
        connection.rollback();
        connection.setAutoCommit(true);
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
