package net.mennia.svsinfo.model;

import java.util.Date;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class NewsItem {

    private int id;
    private String title;
    private String content;
    private Date created;
    private Activity activity;
    private Event event;

    public NewsItem() {
        id = 0;
        title = "";
        content = "";
        created = null;
        activity = null;
        event = null;
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if(other instanceof NewsItem) {
            NewsItem that = (NewsItem)other;
            result = this.id == that.id;
        }
        return result;
    }

    // <editor-fold desc="getter/setter">

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the activity
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * @param activity the activity to set
     */
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Event getEvent() {
        return event;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // </editor-fold>
    
}
