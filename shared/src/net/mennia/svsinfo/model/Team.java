package net.mennia.svsinfo.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Groups guests into teams
 *
 * @author k4emic
 */
public class Team {

    public enum TeamType {
        priority,
        reserve
    }

    private int maxSize;
    private List<Guest> guests;
    private TeamType type;
    private static final int NO_MAX_MEMBERS = 0;
    private String name;

    public Team() {
        this.maxSize = NO_MAX_MEMBERS;
        this.guests = new ArrayList<Guest>();
        this.type = TeamType.priority;
        this.name = "";
    }

    public Team(int maxSize) {
        this.maxSize = maxSize;
        this.guests = new ArrayList<Guest>();
        this.type = TeamType.priority;
        this.name = "";
    }

    public Team(List<Guest> members, TeamType type) {
        this.maxSize = NO_MAX_MEMBERS;
        this.guests = new ArrayList<Guest>(members);
        this.type = type;
        this.name = "";
    }

    public boolean isFull() {
        if (maxSize == NO_MAX_MEMBERS || guests.size() < maxSize) {
            return false;
        } else {
            return true;
        }
    }

    public boolean add(Guest member) {
        if(isFull()) {
            return false;
        } else {
            guests.add(member);
            return true;
        }
    }

    public List<Guest> getGuests() {
        return new ArrayList<Guest>(this.guests);
    }

    public List<TeamMember> getMembers() {
        List<TeamMember> members = new ArrayList<TeamMember>(this.guests.size());

        TeamMember member;
        for(Guest guest : guests) {
            member = new TeamMember(guest, this);
            members.add(member);
        }

        return members;
    }

    public TeamType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
