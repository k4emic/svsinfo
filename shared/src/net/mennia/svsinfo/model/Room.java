package net.mennia.svsinfo.model;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Room {
    
    private String name;
    private int id;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the roomId
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the roomId to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return getName();
    }
    
    @Override
    public boolean equals(Object obj) {
        
        if(obj instanceof Room) {
            
            Room other = (Room)obj;
            boolean same = other.hashCode() == hashCode();
            
            return same;
            
        }
        
        return false;
        
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.id;
        return hash;
    }
    
}
