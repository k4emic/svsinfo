package net.mennia.svsinfo.model;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class TeamMember {

    private Guest guest;
    private Team team;

    public TeamMember(Guest guest, Team team) {
        this.guest = guest;
        this.team = team;
    }

    /**
     * @return the guest
     */
    public Guest getGuest() {
        return guest;
    }

    /**
     * @return the team
     */
    public Team getTeam() {
        return team;
    }

}
