package net.mennia.svsinfo.model;

import java.util.Date;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Guest {
    
    public static String MALE = "M";
    public static String FEMALE = "F";
    
    private int id;
    private String username;
    private String name;
    private String gender;
    private Date birthdate;
    
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns this guests gender. M if Male or F if Female
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * M if Male or F if Female
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * @param birthdate the birthdate to set
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
}
