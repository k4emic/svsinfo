package net.mennia.svsinfo.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Splits participants into randomized teams
 *
 * @author k4emic
 */
public class TeamGenerator {

    /**
     * Calculates how many teams would be generated from the given number of participants and the given size of each team
     * @param participants
     * @param teamSize
     * @throws IllegalArgumentException if one of the two arguments is not > 0
     * @return
     */
    public static int getNoOfTeams(int participants, int teamSize) {

        if(participants <= 0 || teamSize <= 0) {
            throw new IllegalArgumentException("One of the given values were not > 0");
        }

        return (int) Math.ceil((double) participants / teamSize);
    }

    public TeamGenerator() {
        
    }

    /**
     * Generates a list of teams of the priority type and (optionally) a
     * trailing team of the type "reserve". Team members on the priority teams
     * are shuffled randomly and placed sequentially into teams.
     *
     * If there are more participants than the maxParticipants argument allows,
     * it will put the redundant participants into a team at the tail of the
     * list with the type of "reserve".
     *
     * @param activity
     * @param teamSize
     * @return
     */
    public List<Team> generateTeams(List<Signup> signups, int teamSize, int maxParticipants) {

        List<Team> teams = new ArrayList<Team>();

        // list of all guests
        List<Guest> guests = extractGuests(signups);
        List<Guest> priorityGuests;
        List<Guest> reserveGuests;

        if (maxParticipants <= guests.size()) {
            // segment guests
            priorityGuests = guests.subList(0, maxParticipants);
            reserveGuests = guests.subList(maxParticipants, guests.size());
        } else {
            // no need to segment
            priorityGuests = guests;
            reserveGuests = null;
        }

        Collections.shuffle(priorityGuests);

        Team team = null;
        int i = 0;
        // put priority guests into segmented teams
        for(Guest guest : priorityGuests) {
            if(team == null || team.isFull()) {
                i++;
                team = new Team(teamSize);
                team.setName(Integer.toString(i));
                teams.add(team);
            }

            team.add(guest);
        }

        if(reserveGuests != null) {
            i++;
            Team reserveTeam = new Team(reserveGuests, Team.TeamType.reserve);
            teams.add(reserveTeam);
        }

        return teams;

    }

    /**
     * Extracts guests from list of signups
     *
     * @param signups
     * @return
     */
    private List<Guest> extractGuests(List<Signup> signups) {
        List<Guest> guests = new ArrayList<Guest>(signups.size());

        for (Signup signup : signups) {
            guests.add(signup.getGuest());
        }

        return guests;
    }
}
