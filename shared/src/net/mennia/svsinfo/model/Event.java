package net.mennia.svsinfo.model;

import java.util.Date;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Event {
    
    private int id;
    private String name;
    private Date startTime;
    private Date endTime;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the startDate
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startDate to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endDate
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endDate to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
}
