package net.mennia.svsinfo.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Signup {
    
    private Guest guest;
    private Activity activity;

    public Signup(Activity activity, Guest guest) {
        setActivity(activity);
        setGuest(guest);
    }

    /**
     * @return the guest
     */
    public Guest getGuest() {
        return guest;
    }

    /**
     * @param guest the guest to set
     */
    public void setGuest(Guest guest) {
        this.guest = guest;
    }

    /**
     * @return the activity
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * @param activity the activity to set
     */
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    /**
     * Extracts guests from a list of signups
     * @param signups
     * @return
     */
    public static List<Guest> extractGuests(List<Signup> signups) {
        List<Guest> guests = new ArrayList<Guest>(signups.size());

        for(Signup signup : signups) {
            guests.add(signup.getGuest());
        }

        return guests;
    }
    
}
