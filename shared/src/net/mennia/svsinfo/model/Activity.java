package net.mennia.svsinfo.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class Activity {

    public enum SignupState {

        OPEN,
        CLOSED
    }
    private int id;
    private String name;
    private String description;
    private Date timeOfStart;
    private int duration; // duration of the event in minutes
    private int maxParticipants;
    private boolean requiresSignup;
    private Room room;
    private Event event;
    private List<Signup> signups;

    public Activity() {
        id = 0;
        name = "";
        description = "";
        timeOfStart = new Date();
        duration = 0;
        maxParticipants = 0;
        requiresSignup = false;
        signups = new ArrayList<Signup>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the timeOfStart
     */
    public Date getTimeOfStart() {
        return timeOfStart;
    }

    /**
     * @param timeOfStart the timeOfStart to set
     */
    public void setTimeOfStart(Date timeOfStart) {
        this.timeOfStart = timeOfStart;
    }

    /**
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return the maxParticipants
     */
    public int getMaxParticipants() {
        return maxParticipants;
    }

    /**
     * @param maxParticipants the maxParticipants to set
     */
    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    /**
     * @return the requiresSignup
     */
    public boolean getRequiresSignup() {
        return requiresSignup;
    }

    /**
     * @param requiresSignup the requiersSignup to set
     */
    public void setRequiresSignup(boolean requiresSignup) {
        this.requiresSignup = requiresSignup;
    }

    /**
     * Calculates the time for the end of this activity from the duration and
     * returns a date object
     *
     * @return
     */
    public Date getTimeOfEnd() {
        Date start = getTimeOfStart();

        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        cal.add(Calendar.MINUTE, duration);

        return cal.getTime();
    }

    /**
     * @return the room
     */
    public Room getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(Room room) {
        this.room = room;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    List<Signup> getSignups() {
        return new ArrayList<Signup>(signups);
    }

    public void addGuest(Guest guest) {
        Signup signup = new Signup(this, guest);
        signups.add(signup);
    }

    public void setSignups(List<Signup> signups) {
        this.signups = new ArrayList<Signup>(signups);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Activity) {
            Activity other = (Activity) obj;
            return this.id == other.id;
        } else {
            return false;
        }

    }

    /**
     * Returns a SignupState enum to indicate whether or not signups are closed
     * or open
     *
     * @return
     */
    public SignupState getSignupState() {

        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();

        cal.setTime(timeOfStart);
        cal.add(Calendar.MINUTE, -5); // subtract 5 minutes from time of start
        
        Date closedAt = cal.getTime();

        if(now.after(closedAt)) {
            return SignupState.CLOSED;
        } else {
            return SignupState.OPEN;
        }
        
    }
}
