package net.mennia.svsinfo.helpers.date;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public class DateHelper {
    
    /**
     * Combines the date from the date argument and the time from the time argument into a single Date object
     * @param date
     * @param time
     * @return 
     */
    public static Date combineDateTime(Date date, Date time) {
        
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);
        Calendar timeCal = Calendar.getInstance();
        timeCal.setTime(time);
        
        int year = dateCal.get(Calendar.YEAR);
        int month = dateCal.get(Calendar.MONTH);
        int day = dateCal.get(Calendar.DAY_OF_MONTH);
        
        int hour = timeCal.get(Calendar.HOUR_OF_DAY);
        int minute = timeCal.get(Calendar.MINUTE);
        int second = timeCal.get(Calendar.SECOND);
        int mili = timeCal.get(Calendar.MILLISECOND);
        
        Calendar resultCal = Calendar.getInstance();
        
        resultCal.set(year, month, day, hour, minute, second);
        resultCal.set(Calendar.MILLISECOND, mili);
        
        return resultCal.getTime();
    }
    
}
