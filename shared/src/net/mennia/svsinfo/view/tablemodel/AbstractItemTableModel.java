package net.mennia.svsinfo.view.tablemodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Generic class for table models with single objects
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public abstract class AbstractItemTableModel<T> extends AbstractTableModel {
    
    private List<T> items;
    
    public AbstractItemTableModel(Collection<T> items) {
        this.items = new ArrayList<T>(items);
    }
    
    public AbstractItemTableModel() {
        this.items = new ArrayList<T>();
    }

    public void clear() {
        items.clear();
    }
    
    public void setItems(Collection<T> items) {
        this.items = new ArrayList<T>(items);
    }

    public Collection<T> getItems() {
        return new ArrayList<T>(this.items);
    }
    
    public void addItem(T item) {
        items.add(item);
    }
    
    public void removeItem(T item) {
        items.remove(item);
    }
    
    @Override
    public int getRowCount() {
        return items.size();
    }
    
    public T getItemAt(int rowIndex) {
        
        T item = null;
        
        try {
            item = items.get(rowIndex);
        } catch(IndexOutOfBoundsException ex) {
            System.out.println(ex.getMessage());
        }
        
        return item;
        
    }
    
    public int countItems() {
        return items.size();
    }
    
    @Override
    abstract public int getColumnCount();

    @Override
    public abstract Object getValueAt(int rowIndex, int columnIndex);
    
    @Override
    public abstract Class getColumnClass(int columnIndex);
    
    @Override
    public abstract String getColumnName(int columnIndex);    
    
}
