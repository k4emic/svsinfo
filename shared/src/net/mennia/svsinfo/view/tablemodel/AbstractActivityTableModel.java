package net.mennia.svsinfo.view.tablemodel;

import java.util.Collection;
import net.mennia.svsinfo.model.Activity;

/**
 *
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public abstract class AbstractActivityTableModel extends AbstractItemTableModel<Activity> {
    
    public AbstractActivityTableModel() {
        super();
    }
    
    public AbstractActivityTableModel(Collection<Activity> items) {
        super(items);
    }
    
    /**
     * Returns the index of the given activity
     * @param activity
     * @return 
     */
    public int indexOf(Activity activity) {
        
        boolean found = false;
        int index = 0;
        int foundIndex = -1;
        int max = countItems();
        
        Activity act;
        
        while(!found && index < max) {
            
            act = getItemAt(index);
            
            if(act.getId() == activity.getId()) {
                found = true;
                foundIndex = index;
            } else {
                index++;
            }
            
        }
        
        return foundIndex;
        
    }
    
}
