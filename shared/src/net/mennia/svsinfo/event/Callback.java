package net.mennia.svsinfo.event;

/**
 * Multi-purpose interface for passing callback between methods
 * @author Mads Nielsen <k4emic@gmail.com>
 */
public interface Callback {
    
    /**
     * Starts the callback
     */
    public void invoke();
}

